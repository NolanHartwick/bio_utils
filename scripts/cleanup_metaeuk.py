
import pandas
import argparse
from collections import deque, defaultdict, Counter


from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation


def load_data(fasta_path, gff3_path=None):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    if(gff3_path is not None):
        with open(gff3_path) as fin:
            ret = list(GFF.parse(fin, base_dict=seq_dict))
            return {c.id: c for c in ret}
    else:
        return seq_dict


def get_features(features, feat_type):
    toprocess = deque(features)
    genes = []
    while(len(toprocess) > 0):
        next_f = toprocess.popleft()
        if(next_f.type == feat_type):
            genes.append(next_f)
        else:
            for f in next_f.sub_features:
                toprocess.append(f)
    return genes


def rev_comp(s, lang=dict(zip("ACGTNacgt", "TGCANtgca"))):
    return "".join(lang[c] for c in s)[::-1]


def metaeuk_prots_to_feats(genome, prots):
    for i, p in enumerate(prots):
        try:
            T_acc, C_acc, S, bitscore, EValue, number_exons, low_coord, high_coord, *exons = p.split("|")
            start, end = int(low_coord), int(high_coord)
            strand = 1 if(S == "+") else -1
            floc = FeatureLocation(start, end, strand=strand)
            gene = SeqFeature(floc, type="gene", id=f"metaeuk_g{i}", qualifiers={"ID": f"metaeuk_g{i}"})
            qual = {"t_acc": T_acc, "bitscore": bitscore, "evalue": EValue, "ID": f"metaeuk_t{i}"}
            transcript = SeqFeature(floc, type="mRNA", id=f"metaeuk_t{i}", qualifiers=qual)
            gene.sub_features = [transcript]
            transcript.sub_features = []
            for e in exons:
                e = e.split(":")
                s1, s2 = int(e[0].split("[")[0]), int(e[0].split("[")[1][:-1])
                e1, e2 = int(e[1].split("[")[0]), int(e[1].split("[")[1][:-1])
                if(e1 != e2):
                    raise ValueError("weird: " + p)
                if(strand == 1):
                    floc = FeatureLocation(int(s2), int(e1+1), strand=strand)
                else:
                    floc = FeatureLocation(int(e1), int(s2+1), strand=strand)
                cds = SeqFeature(floc, type="CDS", id=f"metaeuk_cds{i}", qualifiers={"ID": f"metaeuk_cds{i}"})
                transcript.sub_features.append(cds)
            genome[C_acc].features.append(gene)
        except:
            print(p.replace("|", "\n"))
            print(exons)
            raise


def get_codons(seq, offset=0):
    temp = iter(seq[offset:])
    return ["".join(t) for t in zip(temp, temp, temp)]


def get_up_down_stream(seqrec_str, t, k):
    cds_feats = [f for f in t.sub_features if(f.type == "CDS")]
    cds_feats = sorted(cds_feats, key=lambda f: f.location.nofuzzy_start * f.strand)
    if(t.strand == 1):
        start = cds_feats[0].location.nofuzzy_start
        end = cds_feats[-1].location.nofuzzy_end
        pre = seqrec_str[start-k:start+3]
        post = seqrec_str[end-3:end+k]
    elif(t.strand == -1):
        start = cds_feats[-1].location.nofuzzy_start
        end = cds_feats[0].location.nofuzzy_end
        pre = rev_comp(seqrec_str[end-3:end+k])
        post = rev_comp(seqrec_str[start-k:start+3])
    return pre, post


def get_cds(seqrec_str, t):
    cds_feats = [f for f in t.sub_features if f.type == "CDS"]
    cds_feats = sorted(cds_feats, key=lambda f: f.location.nofuzzy_start * f.strand)
    cds_seq = "".join(f.extract(seqrec_str) for f in cds_feats)
    return cds_seq


def cds_to_prot(seq_str):
    return str(Seq.Seq(seq_str).translate())


def rescue_models(records, k=50):
    """ evaluate gene models in records, and then try to
        adjust them to find stop/start codons by checking
        up to K codons up stream and downstream
    """
    for ctgid, seqrec in records.items():
        seqrec_str = str(seqrec.seq)
        genes = get_features(seqrec.features, "gene")
        start_codon = "ATG"
        stop_codons = ["TAA", "TAG", "TGA"]
        for g in genes:
            mrna = get_features([g], "mRNA")
            for t in mrna:
                cds = get_features([t], "CDS")
                cds = sorted(cds, key=lambda f: f.location.nofuzzy_start * f.strand)
                cdsseq = get_cds(seqrec_str, t)
                codons = get_codons(cdsseq)
                upstream, downstream = get_up_down_stream(seqrec_str, t, k*3)
                upcodons, downcodons= get_codons(upstream), get_codons(downstream)
                # check start
                upstream_stop_flag = False
                for i in range(min(k, len(codons))):
                    if(codons[i] == start_codon):
                        if(t.strand == 1):
                            fudged = cds[0].location.nofuzzy_start + i * 3
                            if(fudged < cds[0].location.nofuzzy_end):
                                cds[0].location = FeatureLocation(fudged, cds[0].location.nofuzzy_end, strand=t.strand)
                                t.location = FeatureLocation(fudged, t.location.nofuzzy_end, strand=t.strand)
                                g.location = FeatureLocation(fudged, g.location.nofuzzy_end, strand=t.strand)
                                break
                        elif(t.strand == -1):
                            fudged = cds[0].location.nofuzzy_end - i * 3
                            if(fudged > cds[0].location.nofuzzy_start):
                                cds[0].location = FeatureLocation(cds[0].location.nofuzzy_start, fudged, strand=t.strand)
                                t.location = FeatureLocation(t.location.nofuzzy_start, fudged, strand=t.strand)
                                g.location = FeatureLocation(g.location.nofuzzy_start, fudged, strand=t.strand)
                                break
                    if(not upstream_stop_flag):
                        curcodon = upcodons[-1 - i]
                        if(curcodon in stop_codons):
                            upstream_stop_flag = True
                        elif(curcodon == start_codon):
                            if(t.strand == 1):
                                fudged = cds[0].location.nofuzzy_start - i * 3 
                                cds[0].location = FeatureLocation(fudged, cds[0].location.nofuzzy_end, strand=t.strand)
                                t.location = FeatureLocation(fudged, t.location.nofuzzy_end, strand=t.strand)
                                g.location = FeatureLocation(fudged, g.location.nofuzzy_end, strand=t.strand)
                            elif(t.strand == -1):
                                fudged = cds[0].location.nofuzzy_end + i * 3 
                                cds[0].location = FeatureLocation(cds[0].location.nofuzzy_start, fudged, strand=t.strand)
                                t.location = FeatureLocation(t.location.nofuzzy_start, fudged, strand=t.strand)
                                g.location = FeatureLocation(g.location.nofuzzy_start, fudged, strand=t.strand)
                            break
                # check stop
                for i in range(min(k, len(codons))):
                    if(codons[-1 - i] in stop_codons):
                        pass
                    elif(downcodons[i] in stop_codons):
                        if(t.strand == -1):
                            fudged = cds[-1].location.nofuzzy_start - i * 3 
                            cds[-1].location = FeatureLocation(fudged, cds[-1].location.nofuzzy_end, strand=t.strand)
                            t.location = FeatureLocation(fudged, t.location.nofuzzy_end, strand=t.strand)
                            g.location = FeatureLocation(fudged, g.location.nofuzzy_end, strand=t.strand)
                        elif(t.strand == 1):
                            fudged = cds[-1].location.nofuzzy_end + i * 3 
                            cds[-1].location = FeatureLocation(cds[-1].location.nofuzzy_start, fudged, strand=t.strand)
                            t.location = FeatureLocation(t.location.nofuzzy_start, fudged, strand=t.strand)
                            g.location = FeatureLocation(g.location.nofuzzy_start, fudged, strand=t.strand)
                        break


def evaluate_features(records):
    ret = []
    for ctgid, seqrec in records.items():
        seqrec_str = str(seqrec.seq)
        for g in seqrec.features:
            if(g.type != "gene"):
                continue
            for t in g.sub_features:
                if(t.type != "mRNA"):
                    continue
                ret.append({
                    "GID": g.id,
                    "TID": t.id,
                    "MATCH": t.qualifiers.get("t_acc"),
                    "bitscore": t.qualifiers.get("bitscore"),
                    "evalue": t.qualifiers.get("evalue"),
                    "gstart": t.location.nofuzzy_start,
                    "gend": t.location.nofuzzy_end,
                    "chrom": ctgid,
                    "strand": t.strand,
                    "start_end": get_up_down_stream(seqrec_str, t, 50*3),
                    "cds": get_cds(seqrec_str, t)
                })
    return ret


def group_genes(genes):
    groups = []
    cur = None
    cur_end = -1
    for f in genes:
        if(f.location.nofuzzy_start > cur_end):
            if(cur is not None):
                groups.append(cur)
            cur = [f]
            cur_end = f.location.nofuzzy_end
        else:
            cur.append(f)
            cur_end = max(f.location.nofuzzy_end, cur_end)
    if(cur is not None):
        groups.append(cur)
    return groups


def merge_groups(groups, gid_prefix):
    ret = []
    for i, group in enumerate(groups):
        start = min(g.location.nofuzzy_start for g in group)
        end = max(g.location.nofuzzy_end for g in group)
        strand = group[0].strand
        newid = gid_prefix + "g" + str(i)
        qual = {
            "source": "metaeuk",
            "ID": newid,
        } 
        new_gene = SeqFeature(
            FeatureLocation(start, end),
            type="gene",
            strand=strand,
            qualifiers=qual,
            id=newid
        )
        group_trans = [t for g in group for t in get_features(g.sub_features, "mRNA")]
        for j, t in enumerate(group_trans):
            t.qualifiers['Parent'] = []
            t.id = newid + f"_T{j}"
            t.qualifiers['ID'] = t.id
            cds = get_features([t], "CDS")
            for k, p in enumerate(cds):
                p.qualifiers['Parent'] = []
                p.id = newid + f"_P{j}"
                p.qualifiers['ID'] = p.id
        new_gene.sub_features = [s for f in group for s in f.sub_features]
        ret.append(new_gene)
    return ret


def merge_genes(ref, gid_pattern):
    """ merge gene records when overlapping generating new names
    """
    for ctgid, seqrec in ref.items():
        genes = get_features(seqrec.features, 'gene')
        genes = list(sorted(genes, key=lambda g: g.location.nofuzzy_start))
        plus_groups = group_genes([g for g in genes if(g.strand == 1)])
        neg_groups = group_genes([g for g in genes if(g.strand == -1)])
        new_feats = merge_groups(
            plus_groups + neg_groups,
            f"{gid_pattern}.{ctgid}.",
        )
        ref[ctgid].features = new_feats
    return ref


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that processes metaeuk results to produce better gene-prediction-like results"
        )
    )
    parser.add_argument(
        "-m", "--metaeuk",
        help="fasta file of proteins output by metaeuk"
    )
    parser.add_argument(
        "-g", "--genome",
        help="fasta file containing genome that was processed by metaeuk"
    )
    parser.add_argument(
        "-o", "--outbase",
        help="output prefix where output files will be written",
        default="metaeuk.cleaned"
    )
    parser.add_argument(
        "-p", "--gene_prefix",
        help="prefix used for gene model IDs templated as: {gene_prefix}.{contig}.{typenum}",
        default="metaeuk"
    )
    parser.add_argument(
        "-s", "--search_distance",
        help="how many codons up/down stream to search for valid start/stop codons",
        type=lambda s: int(s) + 1,
        default=26
    )
    parser.add_argument(
        "--min_bitscore",
        help="minimum bitscore for 'pass' models",
        default=200,
        type=int
    )
    parser.add_argument(
        "--min_length",
        help="minimum peptide length for 'pass' models",
        default=50,
        type=int
    )
    return parser.parse_args()


def main():
    args = get_args()
    chroms = load_data(args.genome)
    prots = load_data(args.metaeuk)
    metaeuk_prots_to_feats(chroms, prots)
    rescue_models(chroms, args.search_distance)

    temp = pandas.DataFrame(evaluate_features(chroms))
    temp["prot"] = temp.cds.apply(cds_to_prot)
    temp["valid_start"] = temp.start_end.str[0].str[-3:] == "ATG"
    temp["valid_end"] = temp.start_end.str[1].str[:3].isin(["TAA", "TAG", "TGA"])
    temp["no_contained_stop"] = ~temp.prot.str[:-1].str.contains("\*")
    temp["good_bit"] = temp.bitscore.astype(int) >= args.min_bitscore
    temp["good_len"] = temp.prot.apply(len) >= args.min_length
    temp["isvalid"] = temp.valid_start & temp.valid_end & temp.no_contained_stop & temp.good_bit & temp.good_len
    valid_genes = set(temp[temp.isvalid].GID)

    for seqrec in chroms.values():
        seqrec.features = [g for g in seqrec.features if(g.id in valid_genes)]

    merge_genes(chroms, args.gene_prefix)
    temp = pandas.DataFrame(evaluate_features(chroms))
    temp["prot"] = temp.cds.apply(cds_to_prot)
    
    with open(args.outbase + ".gff3", "w") as fout:
        GFF.write(list(chroms.values()), fout)
    with open(args.outbase + ".proteins.fasta", "w") as fout:
        for tid, prot in zip(temp.TID, temp.prot):
            fout.write(f">{tid}\n{prot}\n")


if(__name__ == "__main__"):
    main()

