#!/bin/bash

# Unofficial Bash Strict Mode
set -euo pipefail
IFS=$'\n\t'

# Function to display the usage of the script
usage() {
	    echo "Usage: $0 <Locus> <atgid> <ref> <refid> <feat> <output_directory>"
	        echo "Example: $0 TOM1 AT4G21790 Tarvense Tarv.1014.HPI3.4.g152100.t1 CDS batches/test"
	}

# Check for the correct number of arguments
if [ "$#" -ne 6 ]; then
	    usage
	        exit 1
fi

mkdir -p $6

cquesta_get_seq.py $3*/*fasta $3*/*gff3 $4 --upstream 1000 --downstream 1000 --feature_bound $5 -o $6/$1
gff_to_genbank.py -g $6/$1.$4.gff3 -f $6/$1.$4.fasta >  $6/$1.$4.gbk
gffread $6/$1.$4.gff3 -g $6/$1.$4.fasta -y $6/$1.$4.proteins.fasta -x $6/$1.$4.cds.fasta -w $6/$1.$4.mrna.fasta


