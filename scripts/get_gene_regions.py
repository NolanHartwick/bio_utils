#!/usr/bin/env python3

import argparse
from collections import deque
import sys
from BCBio import GFF
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord


def load_data(fasta_path, gff3_path):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    with open(gff3_path) as fin:
        return list(GFF.parse(fin, base_dict=seq_dict))


def get_children(f, ftype, include_self=True):
    """ Recursively move through sub_features of f and return a set of all
        sub_features matching ftype
    """
    if(include_self):
        to_process = deque([f])
    else:
        to_process = deque(f.sub_features)
    ret = {}
    processed = set()
    while(len(to_process) > 0):
        f = to_process.popleft()
        if(f.type == ftype):
            ret[f.id] = f
        processed.add(f.id)
        for s in f.sub_features:
            if(s.id not in processed):
                to_process.append(s)
    return ret


def get_feats(record, featuretype):
    """ Return a set of features matching featuretype associated with some seqRecord object
    """
    return {gid: g for f in record.features for gid, g in get_children(f, featuretype).items()}


def get_gene_regions(data, uplength, downlength, featuretype):
    ret = []
    for rec in data:
        ctglen = len(rec)
        genes = get_feats(rec, featuretype=featuretype)
        for fid, f in genes.items():
            if(f.strand == -1):
                start = max(0, f.location.nofuzzy_start - downlength)
                end = min(ctglen, f.location.nofuzzy_end + uplength)
                seq = rec.seq[start:end]
                seq = seq.reverse_complement()
            else:
                start = max(0, f.location.nofuzzy_start - uplength)
                end = min(ctglen, f.location.nofuzzy_end + downlength)
                seq = rec.seq[start:end]
            ret.append(SeqRecord(seq, fid, "", ""))
    return ret


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that extracts gene regions from a fasta file based on a provided gff3."
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "fasta",
        help="fasta file matching the gff3"
    )
    parser.add_argument(
        "gff3",
        help="gff3 containing features to use"
    )
    parser.add_argument(
        "-o", "--output",
        default=sys.stdout,
        type=lambda s: open(s, "w") if s != "-" else sys.stdout,
        help="output path. Use '-' for stdout."
    )
    parser.add_argument(
        "-f", "--feature",
        default="gene",
        help="feature to look for to extract region around."
    )
    parser.add_argument(
        "-u", "--upstream",
        default=500,
        type=int,
        help="length upstream of feature to include in region"
    )
    parser.add_argument(
        "-d", "--downstream",
        default=500,
        type=int,
        help="length downstream of feature to include in region"
    )
    args = parser.parse_args()
    return args


def main():
    args = get_args()
    data = load_data(args.fasta, args.gff3)
    genes = get_gene_regions(data, args.upstream, args.downstream, args.feature)
    for g in genes:
        args.output.write(f">{g.id}\n{g.seq}\n")

if(__name__ == "__main__"):
    main()

