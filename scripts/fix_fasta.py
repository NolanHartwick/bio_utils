#!/usr/bin/env python3

import argparse
import sys
import warnings
import os
import doctest
import json
import math
import re


def suffix_float(s):
    """ take suffix into account when converting s to float. suffixes are case
        insensitive. supported suffixes are.

        Suffix : Multiplier
        k : 10 ** 3
        m : 10 ** 6
        g : 10 ** 9
        t : 10 ** 12
        p : 10 ** 15
    """
    mulipliers = {
        "k": 10**3,
        "m": 10**6,
        "g": 10**9,
        "t": 10**12,
        "p": 10**15,
    }
    if(s[-1] in mulipliers):
        return mulipliers[s[-1].lower()] * float(s[:-1])
    else:
        return float(s)


def read_fasta(fpath, sep=" "):
    """ reads a fasta file at fpath and returns a list of id, info,
        sequence tuples representing the file
    """
    with open(fpath) as f:
        return list(parse_fasta(f, sep))


def split_header(s, sep=" "):
    """ Given a fasta header string, split that string into a name and info.
        Trailing white space will be striped. The ID is differentied from the
        rest of the header by splitting on the first instance of sep in s if
        present

        >>> split_header(">chr1 len=2600\n")
        ("chr1", "len=2600")
        >>> split_header(">chr2 len=2000;contigs=4\n")
        ("chr2", "len=2000;contigs=4")
        >>> split_header(">chr3")
        ("chr3", "")
    """
    s = s.rstrip()
    split_index = s.find(sep)
    split_index = len(s) if(split_index == -1) else split_index
    seq_id = s[1:split_index]
    info = s[split_index + 1:]
    return (seq_id, info)


def parse_fasta(f, sep=" "):
    """ Given a readable file like object containing fasta data, iterate
        over the fasta yielding an id, info, sequence tuple for each entry.

        >>> from io import StringIO
        >>> i = StringIO('>chr1\\nAAAAA\\n>chr2\\nCCCCC')
        >>> list(parse_fasta(i))
        [('chr1', '', 'AAAAA'), ('chr2', '', 'CCCCC')]
    """
    line = f.readline()
    if(line != ''):
        seq_id, info = split_header(line, sep)
        body = []
        for line in f:
            if(line[0] == '>'):
                yield (seq_id, info, ''.join(body))
                seq_id, info = split_header(line, sep)
                body = []
            else:
                body.append(line.rstrip())
        yield (seq_id, info, ''.join(body))


def write_fasta(data, fp, sep=" "):
    """ write a fasta file described by data to outpath. If sort, then sort
         by contig id before writing data to outpath

        data
            An iterable representing fasta data. roughly
            [(id, info, sequence), ...]
        fp
            A file like object with "write"
        sep
            A string used to seperate the id from the info in the fasta
            headers. the written headers will be ">{id}" if info=="" else
            ">{id}{sep}{info}"
    """
    for seq_id, info, seq in data:
        fp.write(">")
        fp.write(seq_id)
        if(info != ""):
            fp.write(sep)
            fp.write(info)
        fp.write("\n")
        line_generator = (seq[i:i + 80] for i in range(0, len(seq), 80))
        for line in line_generator:
            fp.write(line)
            fp.write("\n")


def load_json(s):
    with open(s) as fin:
        return json.load(fin)


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that quickly reads a fasta file, sorts, and renames the contigs."
        ),
        epilog=(
            "USAGE:\n\n    python fix_fasta some.fasta --sep ' ' --thresh 0.01 "
            "--chr chr --other contig > new.fasta"
        )
    )
    parser.add_argument(
        "fasta",
        help="fasta file you wish to make modifications to"
    )
    parser.add_argument(
        "-s", "--sep",
        default=" ",
        help="seperator used to split contig ids from contig info"
    )
    parser.add_argument(
        "-t", "--thresh",
        default=0.01,
        type=suffix_float,
        help=(
            "An value used to seperate contigs from chromosomes. If the value is "
            "greater than or equal to 1, interpret it as an int. Contigs with "
            "length greater than value will be labelled chromosomes. If the value "
            "is less than 1, interpret it as a fraction of total sequence length. "
            "For example, if the value is 0.01, then any contig with length "
            "greater than 1 percent of the total sequence length will be called a "
            "chromosome"
        )
    )
    parser.add_argument(
        "-c", "--chr",
        default="chr",
        help=(
            "Prefix used to name chromosomes. Contigs with length greater than "
            "thresh will have name <chr><k> where k is an incrimenting "
            "integer id."
        ),
    )
    parser.add_argument(
        "-o", "--other",
        default="ctg",
        help=(
            "Prefix used to name contigs. Contigs with length less than "
            "thresh will have name <other><k> where k is an incrimenting "
            "integer id."
        ),

    )
    parser.add_argument(
        "-j", "--specials_json",
        default={},
        type=load_json,
        help=(
            "A path to a json file encoding a mapping from previous contig "
            "names to new contig names. Any contig with a name in this "
            "dictionary will use the associated value as a new name. These"
            "contigs wil be placed immediately following the chromosomes"
        )
    )
    parser.add_argument(
        "-n", "--nosort",
        action='store_true',
        help=(
            "If present, current order will be preserved and numbering will "
            "based on current order with prefixes based on threshold and "
            "contig length. Default behavior is to sort by length "
        )
    )
    parser.add_argument(
        "-m", "--map",
        help=(
            "A json file encoding name mappings (new -> old) will be created "
            "at the specified path. Defaults to <fasta>.names.json"
        )
    )
    parser.add_argument(
        "-g", "--gap",
        default=500,
        type=int,
        help=(
            "Replace long consecutive Ns with this many N. Basically collapses "
            "big gaps to a single standard length. Use a value <=0 to disable and "
            "preserve gaps. Default=500"
        )
    )
    parser.add_argument(
        "-f", "--first_num",
        default=1,
        type=int,
        help="when renumbering contigs, what number should we start with"
    )
    args = parser.parse_args()
    if(args.map is None):
        args.map = args.fasta + ".names.json"
    return args


def main():
    """ Load a fasta file, sort it by length, identify chromosomes by length,
        rename contigs using provided templates
    """
    args = get_args()

    # load data and prep template
    data = read_fasta(args.fasta, args.sep)
    ndigits = int(math.log10(len(data))) + 1
    oth_template = "{:0" + str(ndigits) + "d}"
    chr_template = "{}"

    # if sorting desired
    if(not args.nosort):
        data = list(sorted(data, key=lambda e: len(e[2]), reverse=True))

    # compute threshold if needed
    chr_thresh = args.thresh
    if(chr_thresh < 1.0):
        total_len = sum(len(seq) for _, _, seq in data)
        chr_thresh = total_len * chr_thresh
    chr_thresh = int(chr_thresh)

    # compute new names
    ctg_count = args.first_num - 1
    renames = []
    name_map = {}
    for sid, sinfo, seq in data:
        if(sid in args.specials_json):
            newname = args.specials_json[sid]
        elif(len(seq) >= chr_thresh):
            ctg_count += 1
            newname = args.chr + chr_template.format(ctg_count)
        else:
            ctg_count += 1
            newname = args.other + oth_template.format(ctg_count)
        renames.append((newname, sinfo, seq))
        name_map[newname] = sid

    # collapse gaps
    if(args.gap > 0):
        pattern = "N{" + str(args.gap) + ",}"
        renames = [
            (n, i, re.sub(pattern, "N" * args.gap, seq))
            for n, i, seq in renames
        ]


    # write new fasta to stdout
    write_fasta(renames, sys.stdout, args.sep)

    # dump json map
    with open(args.map, "w") as fout:
        json.dump(name_map, fout, indent=4)


if(__name__ == "__main__"):
    main()
