#!/usr/bin/env python3

import pandas as pd
import os
import argparse
import sys


def get_args():
    parser = argparse.ArgumentParser(description=(
        "Combines information from the full_table output of multiple busco "
        "runs into a single table and writes to stdout. Optionally writes a "
        "merged summary to the specified path."
    ))
    parser.add_argument(
        "busco_dir",
        nargs="+",
        help="paths to busco output dir"
    )
    parser.add_argument(
        "--db", "-d",
        help=(
            "Path to ODB10 busco databse used in the busco runs. If provided, "
            "informaiton and links for busco genes will be embedded in output "
            "table"
        )
    )
    parser.add_argument(
        "--summary", "-s",
        help="output path where a summary table will be created if desired"
    )
    return parser.parse_args()


def busco_basename(fpath):
    return os.path.basename(fpath)[4:]


def parse_table(f):
    colnames = ["busco_id", "status"] + list(range(5))
    df = pd.read_csv(f, sep="\t", comment="#", header=None, names=colnames)
    return df.groupby("busco_id")["status"].first()


def main():
    args = get_args()

    bases = [busco_basename(f) for f in args.busco_dir]
    table_paths = [os.path.join(f, f"full_table_{b}.tsv") for f, b in zip(args.busco_dir, bases)]
    df = pd.DataFrame(
        [parse_table(f) for f in table_paths],
        index=bases
    ).transpose()

    if(args.summary is not None):
        df_summary = pd.DataFrame(
            [df[b].value_counts().sort_index() for b in bases]
        ).transpose()
        df_summary = df_summary.rename({"Complete": "Single"})
        df_ps = df_summary / df_summary.sum()
        df_summary.loc["Complete"] = df_summary.loc["Single"] + df_summary.loc["Duplicated"]
        df_ps.loc["Complete"] = df_ps.loc["Single"] + df_ps.loc["Duplicated"]
        merged = pd.concat([df_summary, df_ps], keys=["count", "ratio"], axis=1)
        merged = merged.reorder_levels([1, 0], axis=1).sort_index(axis=1)
        with open(args.summary, "w") as fout:
            merged.to_csv(fout, sep="\t", float_format="%.4f")

    if(args.db is not None):
        db_desc = os.path.join(args.db, "links_to_ODB10.txt")
        desc = pd.read_csv(db_desc, header=None, sep="\t")
        links = {bid: l for bid, l in zip(desc[0], desc[2])}
        summ = {bid: l for bid, l in zip(desc[0], desc[1])}
        df["description"] = pd.Series(summ)
        df["link"] = pd.Series(links)

    df.to_csv(sys.stdout, sep="\t")


if(__name__ == "__main__"):
    main()
