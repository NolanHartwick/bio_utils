#!/usr/bin/env python3

import argparse
import sys
import pandas
import numpy
from Bio import SeqIO


def parse_fasta_to_df(fhandle):
    df = pandas.DataFrame([
        {'ID': rec.id, 'SEQ': str(rec.seq)}
        for rec in SeqIO.parse(fhandle, "fasta")
    ])
    return df


def reverse_compliment(seq, language=dict(zip('ACTG', 'TGAC'))):
    return ''.join(language[c] for c in reversed(seq))

def score(seq, kmers, allow_rc=True):
    if(allow_rc):
        kmers = kmers | set(reverse_compliment(s) for s in kmers)
    lens = set(len(s) for s in kmers)
    ret = numpy.zeros(len(seq))
    for l in lens:
        for i in range(len(seq) - l + 1):
            if(seq[i:i+l] in kmers):
                ret[i] += 1
    return ret


def smooth_data_np_convolve(arr, span):
    return numpy.convolve(arr, numpy.ones(span * 2 + 1) / (span * 2 + 1), mode="same")


def write_bedgraph(data, fout):
    """ data = {contig_id : array}
    """
    for ctg, d in data.items():
        start = 0
        val = d[start]
        stop = 0
        for i, v in enumerate(d):
            if(v != val):
                entry = f"{ctg}\t{start}\t{stop}\t{val}\n"
                if(val != 0):
                    fout.write(entry)
                start = i
                val = v
            stop += 1
        entry = f"{ctg}\t{start}\t{stop}\t{val}\n"
        if(val != 0):
            fout.write(entry)


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "searches for specific kmers and returns wiggle format file showing locations"
        ),
    )
    parser.add_argument(
        "fasta",
        type = lambda f: sys.stdin if(f == '-') else open(f),
        help=(
            "a fasta file to search. Use - to read stdin"
        )
    )
    parser.add_argument(
        'kmers',
        nargs="*",
        type = lambda s: s.upper(),
        help="kmers to search for"
    )
    parser.add_argument(
        '--kmer_file', "-k",
        default=None,
        help="path to a text file containing kmers to search for"
    )
    parser.add_argument(
        '--window_size', '-w',
        default=0,
        type=int,
        help="integer capturing window size for convolutions if desired"
    )
    return parser.parse_args()


def main():
    args = get_args()
    kmers = set(args.kmers)
    if(args.kmer_file is not None):
        with open(args.kmer_file) as fin:
            kmers = kmers | set(k for k in fin.read().split())
    if(len(kmers) == 0):
        raise ValueError("Kmers must be provided either as positional args or via --kmer_file")
    fasta = parse_fasta_to_df(args.fasta)
    fasta['SCORES'] = fasta.SEQ.apply(
        lambda s: smooth_data_np_convolve(score(s, kmers), args.window_size)
    )
    write_bedgraph(dict(zip(fasta.ID, fasta.SCORES)), sys.stdout)


if(__name__ == "__main__"):
    main()


