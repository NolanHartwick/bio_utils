#!/usr/bin/env python3

import pandas
import argparse
import sys

def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "Converts a juicer 'medium' style contacts table into a "
            "long' style table with some dummy values"
        )
    )
    parser.add_argument(
        "contacts",
        help="A table with medium style juicer contacts"
    )
    return parser.parse_args()


def medium_generator(fpath):
    header = "readname str1 chr1 pos1 frag1 str2 chr2 pos2 frag2 mapq1 mapq2"
    header = header.split()
    return pandas.read_csv(fpath, sep="\s+", header=None, names=header, chunksize=10000)


def main():
    args = get_args()
    inpath = args.contacts
    for chunk in medium_generator(inpath):
        chunk["readname1"] = chunk.readname + "_1"
        chunk["readname2"] = chunk.readname + "_2"
        chunk["cigar1"] = "."
        chunk["cigar2"] = "."
        chunk["sequence1"] = "."
        chunk["sequence2"] = "."
        long_form_keys = """
            str1 chr1 pos1 frag1
            str2 chr2 pos2 frag2
            mapq1 cigar1 sequence1
            mapq2 cigar2 sequence2
            readname1 readname2
        """.split()
        chunk[long_form_keys].to_csv(sys.stdout, sep=" ", header=None, index=None)


if(__name__ == "__main__"):
    main()
