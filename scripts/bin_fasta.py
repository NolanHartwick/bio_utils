#!/usr/bin/env python3

import argparse
from collections import Counter
import gzip

import pandas
import numpy
from Bio import SeqIO, bgzf



base_for = "ACGT"
base_rev = "TGCA"
comp_tab = str.maketrans(base_for, base_rev)

def kmer_count(strings, k, comp_tab=comp_tab):
    ret = Counter()
    for s in strings:
        for i in range(len(s) - k + 1):
            kmer = s[i:i+k]
            rc = kmer.translate(comp_tab)[::-1]
            if(kmer > rc):
                kmer = rc
            ret[kmer] += 1
    return ret


def load_fasta(fasta_path):
    with open(fasta_path) as fin:
        ret = [str(r.seq).upper() for r in SeqIO.parse(fin, "fasta")]
    return ret


def process(fasta, kmers1, kmers2, k, comp_tab=comp_tab):
    """ returns table with columns:
            ID : a specific read id
            HAP1 : count of kmers in fastq read that are in kmers1 and not kmers2
            HAP2 : count of kmers in fastq read that are in kmers2 and not kmers1
            BOTH : count of kmers in fastq read that are in kmers1 and kmers2
            NONE : count of kmers in fastq read that are in neither kmers1 and kmers2
    """
    ret = {}
    with open(fasta, "r") as fin:
        for r in SeqIO.parse(fin, "fasta"):
            seq = str(r.seq).upper()
            cur = {(True, True): 0, (True, False): 0, (False, True): 0, (False, False): 0}
            for i in range(len(seq) - k + 1):
                kmer = seq[i:i+k]
                rc = kmer.translate(comp_tab)[::-1]
                if(kmer > rc):
                    kmer = rc
                flag1 = kmer in kmers1
                flag2 = kmer in kmers2
                cur[(flag1, flag2)] += 1
            ret[r.name] = cur
    ret = pandas.DataFrame(ret).transpose()
    ret.columns = ["BOTH", "HAP1", "HAP2", "NONE"]
    return ret


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "given hap1 and hap2, bin hic fastq1 and fastq2"
        )
    )
    parser.add_argument(
        "hap1",
        help="fasta file representing haplotype1"
    )
    parser.add_argument(
        "hap2",
        help="fasta file representing haplotype1"
    )
    parser.add_argument(
        "fasta_reads",
        help="fasta file containing reads"
    )
    parser.add_argument(
        "outbase",
        help="output prefix"
    )
    parser.add_argument(
        "-m", "--min_hapmers",
        help="a minimum number of hapmers needed to bin a read",
        default=5,
        type=int
    )
    parser.add_argument(
        "-f", "--fold_change",
        help="an adjusted log2 fold change needed to call something as hap1 or hap2",
        default=0.5,
        type=float
    )
    parser.add_argument(
        "-k", "--kmersize",
        help="kmer size to use",
        default=31,
        type=int
    )
    return parser.parse_args()


def bin_hapmers(hapmers, minhaps, min_logratio):
    log_ratio = (hapmers['HAP1'] + 1) / (hapmers['HAP2'] + 1)
    log_ratio = log_ratio.apply(numpy.log2)
    flag1 = log_ratio >= min_logratio
    flag2 = log_ratio <= -1 * min_logratio
    hapflag = hapmers[["HAP1", "HAP2"]].sum(axis=1) < minhaps
    flagtable = {(True, False): 1, (False, True): 2, (False, False): -2}
    hapmers['BIN_LABEL'] = [
        -1 if(hf) else flagtable[(f1, f2)]
        for f1, f2, hf in zip(flag1, flag2, hapflag)
    ]
    return hapmers


def main():
    args = get_args()
    kmers1 = kmer_count(load_fasta(args.hap1), args.kmersize)
    kmers2 = kmer_count(load_fasta(args.hap2), args.kmersize)
    hapmers = process(args.fasta_reads, kmers1, kmers2, args.kmersize)
    hapmers = bin_hapmers(hapmers, args.min_hapmers, args.fold_change)
    hapmers.to_csv(args.outbase + ".hapmer_counts.tsv", sep="\t")
    bin_ret = hapmers["BIN_LABEL"]

    with open(args.fasta_reads, "r") as fin1, \
        open(args.outbase + ".hap1.fasta", "w") as out1, \
        open(args.outbase + ".hap2.fasta", "w") as out2:
        for rec in SeqIO.parse(fin1, "fasta"):
            label = bin_ret[rec.name]
            if(label < 0):
                SeqIO.write(sequences=[rec], handle=out1, format="fasta")
                SeqIO.write(sequences=[rec], handle=out2, format="fasta")
            elif(label == 1):
                SeqIO.write(sequences=[rec], handle=out1, format="fasta")
            elif(label == 2):
                SeqIO.write(sequences=[rec], handle=out2, format="fasta")
            else:
                raise ValueError(f"fuck this label: {label}")


if(__name__ == "__main__"):
    main()

