#!/usr/bin/env python3

from collections import Counter, deque
import argparse
import json
import sys

from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord


def load_gff(gff3_path):
    with open(gff3_path) as fin:
        return list(GFF.parse(fin))


def sort_features(feats):
    return list(sorted(feats, key=lambda f: f.location.nofuzzy_start))


def get_children(f, ftype, include_self=True):
    """ Recursively move through sub_features of f and return a set of all
        sub_features matching ftype
    """
    if(include_self):
        to_process = deque([f])
    else:
        to_process = deque(f.sub_features)
    ret = []
    processed = set()
    while(len(to_process) > 0):
        f = to_process.popleft()
        if(f.type == ftype):
            ret.append(f)
        processed.add(f.id)
        for s in f.sub_features:
            if(s.id not in processed):
                to_process.append(s)
    return ret


def get_genes(record, feattype):
    """ Return a set of features with given feature type for some seqRecord
    """
    return {g.id: g for f in record.features for g in get_children(f, feattype)}


def get_intron_lengths(features, grouptype="mRNA", feattype="CDS"):
    ret = []
    genes = [g for f in features for g in get_children(f, grouptype)]
    for g in genes:
        cds = get_children(g, feattype)
        cds = sort_features(cds)
        if(len(cds) > 1):
            for i in range(len(cds) - 1):
                ret.append(cds[i+1].location.nofuzzy_start - cds[i].location.nofuzzy_end)
    return ret


def get_subfeature_count(features, grouptype="mRNA", feattype="CDS"):
    ret = []
    genes = [g for f in features for g in get_children(f, grouptype)]
    for g in genes:
        cds = get_children(g, feattype)
        cds = sort_features(cds)
        ret.append(len(cds))
    return ret


def get_feature_lengths(features, feattype):
    return  [
        g.location.nofuzzy_end - g.location.nofuzzy_start
        for f in features
        for g in get_children(f, feattype)
    ]


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "write json to stdout with stats describing gene models in gff file"
        )
    )
    parser.add_argument(
        "gff",
        help="some gff"
    )
    parser.add_argument(
        "--gene_feature",
        default="gene",
        help="feature type to look for to identify gene features. Default is 'gene'"
    )
    parser.add_argument(
        "--cds_feature",
        default="CDS",
        help="feature type to look for to identify CDS features. Should be children of --mran_feature. Default is 'CDS'"
    )
    parser.add_argument(
        "--mrna_feature",
        default="mRNA",
        help="feature type to look for to identify transcript features. Should be children of --gene_feature. Default is 'mRNA'"
    )
    return parser.parse_args()


def main():
    args = get_args()
    seqs = load_gff(args.gff)
    ret = {
        "CDS_lengths": Counter(l for s in seqs for l in get_feature_lengths(s.features, args.cds_feature)),
        "gene_lengths": Counter(l for s in seqs for l in get_feature_lengths(s.features, args.gene_feature)),
        "intron_lengths": Counter(l for s in seqs for l in get_intron_lengths(s.features, args.mrna_feature, args.cds_feature)),
        "cds_counts": Counter(l for s in seqs for l in get_subfeature_count(s.features,  args.mrna_feature, args.cds_feature))
    }
    json.dump(ret, sys.stdout, indent=4)


if(__name__ == "__main__"):
    main()
