#!/usr/bin/env python3

import argparse
import pandas
import sys


def parse_filter(s):
    sign, taxid, thresh = s.split(":")
    thresh = float(thresh)
    taxid = taxid.lower()
    if(sign != "i" and sign != "x"):
        raise ValueError(f"improper filter expression : {s}")
    return {
        "type": sign,
        "taxid": taxid,
        "threshold": thresh 
    }


def get_args():
    """ Parse arguments for the cli version of subset_fastq
    """
    parser = argparse.ArgumentParser(
        description=(
            "split a fasta file based on eggnog annotations of proteins by applying "
            "a series of filters. 'i' (include) operations add sequences to the output. "
            "'x' (exclude) operations remove sequences from the output. If the "
            "first filter is an exclude, all sequences will "
            "be added prior to applying the filter. If the first filter is an include, "
            "no sequences will be added prior to applying the filter. This tool is "
            "only compatible with salk-tm genomes/annotations. "
        )
    )
    parser.add_argument(
        "fasta",
        type=lambda f: sys.stdin if(f == "-") else open(f),
        help="path to fasta file whos proteins were annotated by eggnog or '-' to read from stdin"
    )
    parser.add_argument(
        "eggnog",
        help="path to eggnog annotations"
    )
    parser.add_argument(
        "filter",
        nargs="+",
        type=parse_filter,
        help=(
            "a series of filter expressions. Format should match x|i:<taxid>:0.0-1.0. "
            "Should look like... i:Viridiplantae:0.25 x:Bacteria:0.5"
        )
    )
    parser.add_argument(
        "-s", "--stats",
        default=None,
        help="If present, taxid counts for each contig will be written to this location"
    )
    parser.add_argument(
        "-i", "--include_nulls",
        action="store_true",
        help="If present, contigs with no annotated gene models will be included in the output"
    )
    return parser.parse_args()


def apply_filter(ratios, includes, excludes, filter):
    """ apply some filter to 
    """
    includes, excludes = includes.copy(), excludes.copy()
    taxid = filter["taxid"]
    thresh = filter["threshold"]
    ftype = filter["type"]
    if(taxid not in ratios.columns):
        raise ValueError(f"Unrecognized taxid: {taxid}")
    for ctg in ratios.index[ratios[taxid] >= thresh]:
        if(ftype == "x" and ctg in includes):
            includes.remove(ctg)
            excludes.add(ctg)
        elif(ftype == "i" and ctg in excludes):
            excludes.remove(ctg)
            includes.add(ctg)
    return includes, excludes
            

def filter_fasta(fin, includes, excludes, include_nulls):
    joint = includes | excludes
    for line in fin:
        if(line.startswith(">")):
            ctgid = line[1:-1].split(" ")[0]
            if(ctgid in includes):
                dowrite = True
            elif(include_nulls and ctgid not in joint):
                dowrite = True
            else:
                dowrite = False
        if(dowrite):
            sys.stdout.write(line)


def main():
    args = get_args()
    eggnog = pandas.read_csv(args.eggnog, sep="\t")
    eggnog["ctg"] = eggnog.query_name.str.split("\.g").str[0].str.split("\.").str[-1]
    eggnog["taxonomic scope"] = eggnog["taxonomic scope"].str.lower()
    counts = eggnog.groupby("ctg")["taxonomic scope"].value_counts().unstack().fillna(0).astype(int)
    if(args.stats is not None):
        counts.to_csv(args.stats, sep="\t")
    ratios = counts.div(counts.sum(axis=1), axis=0)
    if(args.filter[0]["type"] == "x"):
        includes, excludes = set(ratios.index), set()
    else:
        excludes, includes = set(ratios.index), set()
    for f in args.filter:
        includes, excludes = apply_filter(ratios, includes, excludes, f)
    filter_fasta(args.fasta, includes, excludes, args.include_nulls)


if(__name__ == "__main__"):
    main()
