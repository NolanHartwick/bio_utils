#!/usr/bin/env python3

import argparse
import os
import sys
from collections import deque
import json
import pandas
from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord


def load_data(fasta_path, gff3_path):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    with open(gff3_path) as fin:
        return list(GFF.parse(fin, base_dict=seq_dict))


def get_children(f, ftype, include_self=True):
    """ Recursively move through sub_features of f and return a set of all
        sub_features matching ftype
    """
    if(include_self):
        to_process = deque([f])
    else:
        to_process = deque(f.sub_features)
    ret = set()
    processed = set()
    while(len(to_process) > 0):
        f = to_process.popleft()
        if(f.type == ftype):
            ret.add(f)
        processed.add(f)
        for s in f.sub_features:
            if(s not in processed):
                to_process.append(s)
    return ret


def get_seq(record, feature):
    return str(record.seq[feature.location.nofuzzy_start: feature.location.nofuzzy_end])


def get_genes(record, keys):
    """ Return a set of genes associated with some seqRecord object
    """
    return set(g for f in record.features for g in get_children(f, keys["gene"]))


def sort_features(feats):
    return list(sorted(feats, key=lambda f: f.location.nofuzzy_start))


def primary_cds_and_prots(data, keys):
    """extracts the cds sequence associated with the longest transcript associated with each gene"""
    exon_records = []
    for rec in data:
        genes = get_genes(rec, keys)
        if(len(genes) == 0):
            continue
        df = pandas.DataFrame([
            {"gene": g, "transcript": t, "gid": g.id, "tid": t.id}
            for g in genes
            for t in get_children(g, keys["mRNA"])
        ])
        if(len(df) == 0):
            continue
        df["EXONS"] = df.transcript.apply(lambda t: sort_features(get_children(t, keys["exon"])))
        df = df[df.EXONS.apply(len) != 0]
        df["strand"] = df.EXONS.apply(lambda feats: feats[0].strand != -1)
        df["niave_cds_seq"] = df.EXONS.apply(lambda exons: "".join(get_seq(rec, f) for f in exons))
        df["cds_seq"] = [
            Seq.Seq(seq) if(strand) else Seq.Seq(seq).reverse_complement()
            for seq, strand in zip(df.niave_cds_seq, df.strand)
        ]
        df["cds_record"] = [SeqRecord(s, t.id, "", "") for t, s in zip(df.transcript, df.cds_seq)]
        df["cds_len"] = df.cds_seq.apply(len)
        df["transcript_len"] = df.transcript.apply(lambda t: t.location.nofuzzy_end - t.location.nofuzzy_start)
        
        primary_df = df.groupby("gid").apply(
            lambda df: df.sort_values(["cds_len", "transcript_len"]).iloc[-1]
        )
        exon_records.extend(list(primary_df.cds_record))
    return exon_records


def cli_main():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that extracts exon sequences from a fasta file based on a provided gff3."
        )
    )
    parser.add_argument(
        "fasta",
        help="fasta file matching the gff3"
    )
    parser.add_argument(
        "gff3",
        help="gff3 containing features to use"
    )
    parser.add_argument(
        "--gene_feature",
        default="gene",
        help="feature type to look for to identify gene features. Default is 'gene'"
    )
    parser.add_argument(
        "--mrna_feature",
        default="mRNA",
        help="feature type to look for to identify transcript features. Should be children of --gene_feature. Default is 'mRNA'"
    )
    parser.add_argument(
        "--exon_feature",
        default="exon",
        help="feature type to look for to identify exon features. Should be children of --mrna_feature. Default is 'exon'"
    )
    args = parser.parse_args()
    keys = {
        "mRNA": args.mrna_feature,
        "gene": args.gene_feature,
        "exon": args.exon_feature
    }
    get_exons(
        args.fasta,
        args.gff3,
        keys
    )


def get_exons(fasta_path, gff3_path, keys):
    """ generates exon sequences
    """
    data = load_data(fasta_path, gff3_path)
    exons = primary_cds_and_prots(data, keys)
    SeqIO.write(exons, sys.stdout, "fasta")


if(__name__ == "__main__"):
    cli_main()
