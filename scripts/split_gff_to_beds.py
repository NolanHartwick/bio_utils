#!/usr/bin/env python3
import argparse
import sys

import pandas


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            'read a gff3 and split it into bed files based on the feature types'
        )
    )
    parser.add_argument(
        "gff3",
        type=lambda s: sys.stdin if(s == "-") else open(s),
        help="path to gff3 file, use - for stdin"
    )
    parser.add_argument(
        "-o", "--outbase",
        help="prefix for output files"
    )
    return parser.parse_args()


def main():
    args = get_args()
    gff = pandas.read_csv(args.gff3, sep="\t", header=None, comment="#")
    gff[9] = "."
    gff[10] = gff[8].str.replace(r"^.*ID=(.*?);.*$", lambda m: m.group(1), regex=True)
    gff[11] = 1
    stdoutlist = []
    for k, df in gff.groupby(2):
        df[[0, 3, 4, 10, 11, 6]].to_csv(f"{args.outbase}.{k}.bed", sep="\t", header=None, index=None)
        stdoutlist.append(k)
    print("\t".join(stdoutlist))


if(__name__=="__main__"):
    main()
