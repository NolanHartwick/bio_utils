#!/usr/bin/env python3

import argparse
from collections import defaultdict

import pandas
from Bio import SeqIO


def find_related_nodes(start_node, relationship_map):
    related_nodes = []
    nodes_to_visit = [start_node]
    while nodes_to_visit:
        current = nodes_to_visit.pop()
        next_nodes = relationship_map.get(current, [])
        related_nodes.extend(next_nodes)
        nodes_to_visit.extend(next_nodes)
    return related_nodes


def reverse_map(m):
    r = defaultdict(list)
    for n, parents in m.items():
        for p in parents:
            r[p].append(n)
    return dict(r)


def load_gff3(gffpath):
    df = pandas.read_csv(gffpath, sep="\t", comment="#", header=None)
    df.columns = 'SEQID SOURCE TYPE START END SCORE STRAND PHASE ATTR'.split()
    df['ID'] = df['ATTR'].str.extract(r'ID=([^;]+)')
    df['Parent'] = df['ATTR'].str.extract(r'Parent=([^;]+)')
    return df
    

def load_fasta(fasta_path):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    return seq_dict
    

def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that extracts biological sequences from a fasta "
            "file based on a provided gff3. Formated for Cquesta"
        )
    )
    parser.add_argument(
        "fasta",
        help="fasta file matching the gff3"
    )
    parser.add_argument(
        "gff3",
        help="gff3 containing features to use"
    )
    parser.add_argument(
        "mRNA",
        nargs="+",
        help="mRNA features to extract"
    )
    parser.add_argument(
        "-o", "--outbase",
        default="selected_region",
        help="a prefix to use for generating output files"
    )
    parser.add_argument(
        "--upstream",
        default=2000,
        type=int,
        help="default length for promoter region to extract"
    )
    parser.add_argument(
        "--downstream",
        default=1000,
        type=int,
        help="default length for terminal region to extract"
    )
    parser.add_argument(
        "--feature_bound",
        default="CDS",
        help="feature type to consider when determining feature boundaries"
    )
    args = parser.parse_args()
    return args


def main():
    args = get_args()
    data = load_gff3(args.gff3)
    seqs = load_fasta(args.fasta)
    feat_to_parent = data.groupby("ID").Parent.apply(
        lambda x: x.dropna().unique().tolist()
    ).to_dict()
    feat_to_children = reverse_map(feat_to_parent)
    feat_to_descendants = {
        node: find_related_nodes(node, feat_to_children)
        for node in feat_to_parent
    }
    feat_to_ancestors = {
        node: find_related_nodes(node, feat_to_parent)
        for node in feat_to_parent
    }
    feat_ids = set(data.ID)
    # print(data)
    for f in args.mRNA:
        if(f not in feat_ids):
            print(f"Feature with ID={f} not found:")
            continue
        descendants = feat_to_descendants[f]
        temp = data[data.ID.isin([f] + descendants + feat_to_parent[f])].copy()
        ctg = str(temp.SEQID.iloc[0])
        maxlen = len(seqs[ctg].seq)
        if(temp.STRAND.iloc[0] == "+"):
            start = temp[temp.TYPE == args.feature_bound].START.min()
            end = temp[temp.TYPE == args.feature_bound].END.max()
            newstart = start - args.upstream if(args.upstream < start) else 0
            newend = end + args.downstream if(args.downstream + end < maxlen) else maxlen
        else:
            start = temp[temp.TYPE == args.feature_bound].START.min()
            end = temp[temp.TYPE == args.feature_bound].END.max()
            newstart = start - args.downstream if(args.downstream < start) else 0
            newend = end + args.upstream if(args.upstream + end < maxlen) else maxlen
        newctg = seqs[str(temp.SEQID.iloc[0])].seq[newstart:newend]
        newctgid = f"{temp.SEQID.iloc[0]}_{newstart}_{newend}"
        temp.START = temp.START - newstart
        temp.END = temp.END - newstart
        temp.SEQID = newctgid
        temp.iloc[:, :9].to_csv(f"{args.outbase}.{f}.gff3", sep="\t", header=None, index=None)
        with open(f"{args.outbase}.{f}.fasta", "w") as fout:
            fout.write(f">{newctgid}\n")
            fout.write(str(newctg))


if(__name__ == "__main__"):
    main()
