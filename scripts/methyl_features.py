#!/usr/bin/env python3

from collections import deque, defaultdict
from statistics import stdev, median, mean
import argparse
import sys

import pandas
from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord


def load_ref(fasta_path, gff3_path):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    with open(gff3_path) as fin:
        return list(GFF.parse(fin, base_dict=seq_dict))


def load_methylbed(fpath):
    names = [
        'CTG', 'START', 'STOP', 'NAME', 'SCORE', 'STRAND',
        'THICK_START', 'THICK_END', 'RGB', 'BLOCK_COUNT', 'BLOCK_SIZES'
    ]
    df = pandas.read_csv(fpath, sep="\t", header=None, names=names)
    return df


def get_children(f, ftype, include_self=True):
    """ Recursively move through sub_features of f and return a set of all
        sub_features matching ftype
    """
    if(include_self):
        to_process = deque([f])
    else:
        to_process = deque(f.sub_features)
    ret = set()
    processed = set()
    while(len(to_process) > 0):
        f = to_process.popleft()
        if(f.type == ftype):
            ret.add(f)
        processed.add(f)
        for s in f.sub_features:
            if(s not in processed):
                to_process.append(s)
    return ret


def get_feature_regions(gene, length):
    cds = get_children(gene, 'CDS')
    feats = []
    if(len(cds) == 0):
        return feats
    cds_start = min(f.location.nofuzzy_start for f in cds)
    cds_end = max(f.location.nofuzzy_end for f in cds)
    feats.append((gene.id, 'body', cds_start, cds_end, gene.strand))
    if(gene.strand == 1):
        feats.append((gene.id, 'upstream', cds_start - length, cds_start, gene.strand))
        feats.append((gene.id, 'downstream', cds_end, cds_end + length, gene.strand))
    else:
        feats.append((gene.id, 'downstream', cds_start - length, cds_start, gene.strand))
        feats.append((gene.id, 'upstream', cds_end, cds_end + length, gene.strand))
    return feats
    

def get_genes(record):
    """ Return a set of genes associated with some seqRecord object
    """
    return set(g for f in record.features for g in get_children(f, 'gene'))


def stats_from_bed_entries(beds, k):
    ns = len(beds)
    highs = [b for b in beds if(b.BLOCK_COUNT >= k)]
    lhighs = len(highs)
    ret = {
        "max_cov": None if(ns == 0) else max(b.BLOCK_COUNT for b in beds),
        "mean_cov": None if(ns == 0) else mean(b.BLOCK_COUNT for b in beds),
        "med_cov": None if(ns == 0) else median(b.BLOCK_COUNT for b in beds),
        "err_cov": None if(ns < 2) else stdev(b.BLOCK_COUNT for b in beds),
        "total_cov": None if(ns == 0) else sum(b.BLOCK_COUNT for b in beds),
        "methyl_cov": None if(ns == 0) else sum(int(round(b.BLOCK_COUNT * b.BLOCK_SIZES / 100))  for b in beds),
        "n_sites": ns,
        "high_cov_sites": lhighs,
        "mean_methyl_per": None if(lhighs == 0) else mean(b.BLOCK_SIZES for b in highs),
        "err_methyl_per": None if(lhighs < 2) else stdev(b.BLOCK_SIZES for b in highs)
    }
    return ret


def group_sites(ref, bed, ksize, depth):
    regions = pandas.DataFrame([
        {'CTG': r.id, 'GID': gid, 'RT': rname, 'START': s, 'END': e, 'STRAND': f}
        for r in ref
        for g in get_genes(r)
        for gid, rname, s, e, f in get_feature_regions(g, ksize)
    ]).sort_values(['CTG', 'START'])
    bed = bed.sort_values(['CTG', 'START'])
    region_map = defaultdict(lambda : [])
    iter_reg = regions.iterrows()
    curr = []
    next_idx, next_reg = next(iter_reg)
    i = 0
    for bed_idx, bed_entry in bed.iterrows():
        i+=1
        if(i%10000 == 0):
            sys.stderr.write(f"{i}\n")
        curr = [
            (idx, entry) for idx, entry in curr
            if bed_entry.CTG == entry.CTG and bed_entry.START < entry.END
        ]
        while(next_reg is not None and bed_entry.CTG >= next_reg.CTG and bed_entry.START >= next_reg.START):
            curr.append((next_idx, next_reg))
            try:
                next_idx, next_reg = next(iter_reg)
            except StopIteration:
                next_reg, next_idx = None, None
        for idx, _ in curr:
            region_map[idx].append(bed_entry)
    region_stats = pandas.DataFrame(
        {i: stats_from_bed_entries(beds, depth) for i, beds in region_map.items()}
    ).transpose()
    return regions.merge(region_stats, left_index=True, right_index=True, how='outer')


def cli():
    parser = argparse.ArgumentParser(
        description=(
            "computes gene level features for a given bed"
        )
    )
    parser.add_argument(
        "methylbed",
        help="a methylbed file from megalodon"
    )
    parser.add_argument(
        "fasta",
        help="the reference genome that the methylbed is paired with"
    )
    parser.add_argument(
        "gff",
        help="gff3 file paired with the fasta"
    )
    parser.add_argument(
        '--size', "-s",
        help="number of basepairs up/down stream of genes to feature-ize",
        default=1000,
        type=int
    )
    parser.add_argument(
        '--depth', "-d",
        help="the minimum depth for a call to be considered high-cov",
        default=5,
        type=int
    )
    args = parser.parse_args()
    ref = load_ref(args.fasta, args.gff)
    bed = load_methylbed(args.methylbed)
    ret = group_sites(ref, bed, args.size, args.depth)
    ret.to_csv(sys.stdout, sep="\t", index=None)


if(__name__ == "__main__"):
    cli()
