#!/usr/bin/env python3

import argparse
import sys


def get_args():
    """ Parse arguments for the cli version of subset_fastq
    """
    parser = argparse.ArgumentParser(
        description=(
            'This program will invert a paf file, swapping query and target'
        )
    )
    parser.add_argument(
        "paf",
        help="path to paf file"
    )
    return parser.parse_args()


def main():
    paf = get_args().paf
    with open(paf) as fin:
        for line in fin:
            line = line.split("\t")
            line = line[5:9] + line[4:5] + line[0:4] + line[9:]
            sys.stdout.write('\t'.join(line))


if(__name__ == "__main__"):
    main()
