#!/usr/bin/env python3
import pandas
import argparse
import sys


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "convert gff3 to bedfile"
        )
    )
    parser.add_argument(
        "gff3",
        help="a gff3 file to process"
    )
    parser.add_argument(
        'feature_type',
        nargs="+",
        help="Feature types to convert"
    )
    parser.add_argument(
        "--outpath",
        type=lambda s: open(s, "w"),
        default=sys.stdout,
        help="path to write output too. default=stdout"
    )
    return parser.parse_args()


def gff3_to_bed(gff3_df, feature_types):
    gff3_df['ID'] = gff3_df[8].str.split(";").apply(
        lambda l: [e for e in l if e.startswith("ID=")]
    ).str[0].str[3:]
    filt = gff3_df[gff3_df[2].isin(feature_types)]
    ret = pandas.DataFrame([
        {'ctg': ctg, 'start': start, 'stop': stop, "name": id, 'score': 0, "strand": strand}
        for ctg, start, stop, strand, id in zip(
            filt[0], filt[3], filt[4], filt[6], filt.ID
        )
    ])
    return ret


def main():
    args = get_args()
    gff3 = pandas.read_csv(args.gff3, comment="#", header=None, sep="\t")
    ret = gff3_to_bed(gff3, args.feature_type)
    ret.to_csv(args.outpath, sep="\t", header=None, index=None)


if(__name__=="__main__"):
    main()
