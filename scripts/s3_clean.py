#!/usr/bin/env python3

import boto3
import argparse
from urllib.parse import urlparse
import pandas
import sys
from datetime import datetime, timedelta
import pytz


def list_objects(bucket_name, prefix):
    s3_client = boto3.client('s3')
    paginator = s3_client.get_paginator('list_objects_v2')

    for page in paginator.paginate(Bucket=bucket_name, Prefix=prefix):
        for obj in page.get('Contents', []):
            yield obj


def delete_s3_objects(s3_uris):
    s3_client = boto3.client('s3')

    # Process the list of URIs and prepare the delete request
    delete_requests = {}

    for uri in s3_uris:
        parts = uri.split('/', 3)
        if len(parts) < 4:
            continue  # Skip if URI is not correctly formatted
        bucket_name, key = parts[2], parts[3]
        if bucket_name not in delete_requests:
            delete_requests[bucket_name] = {'Objects': []}
        delete_requests[bucket_name]['Objects'].append({'Key': key})

    responses = []
    for bucket_name, requests in delete_requests.items():
        # Handle more than 1000 objects by splitting into batches
        for i in range(0, len(requests['Objects']), 1000):
            batch = requests['Objects'][i:i+1000]
            response = s3_client.delete_objects(Bucket=bucket_name, Delete={'Objects': batch})
            responses.append(response)
            # Check for errors in the response
            if 'Errors' in response:
                for error in response['Errors']:
                    sys.stderr.write(f"Error deleting object {error['Key']}: {error['Message']}\n")

    return responses


def format_s3_uri(bucket, key):
    return f"s3://{bucket}/{key}"


def multi_list_objects(prefixes):
    objects = []
    for s3prefix in prefixes:
        parsed_uri = urlparse(s3prefix)
        bucket_name = parsed_uri.netloc
        prefix = parsed_uri.path.lstrip('/')
        for obj in list_objects(bucket_name, prefix):
            objects.append({
                "SIZE": obj['Size'],
                "TIMESTAMP": obj['LastModified'],
                "URI": format_s3_uri(bucket_name, obj['Key']),
                "FULL_CHECKSUM": obj['ETag'].strip('"')
            })
    objects = pandas.DataFrame(objects)
    objects["CHECKSUM"] = objects.FULL_CHECKSUM.str.split("-").str[0]
    objects["NAME"] = objects.URI.str.split("/").str[-1]
    return objects


def get_args():
    parser = argparse.ArgumentParser(
        description='Clean duplicate S3 objects with some prefixes'
    )
    parser.add_argument(
        's3_uri',
        help='S3 URI prefix (e.g., s3://my-bucket/my-prefix)',
        nargs="+"
    )
    parser.add_argument(
        "-p", "--ignorepath",
        help="An optional path to a list of s3uri to ignore",
        default=None
    )
    parser.add_argument(
        "-i", "--ignore",
        help="a regex specifying paths to ignore",
        default="s3://salk-tm-raw/reads/[^/]*/[^/]*"
    )
    parser.add_argument(
        "-d", "--dryrun",
        help="if present, only report what deletes would occur",
        default=False,
        action="store_true"
    )
    parser.add_argument(
        "-a", "--min_age",
        help="a minimum age in days an object needs to be to be deleted",
        default=60,
        type=int
    )
    parser.add_argument(
        "-s", "--min_size",
        help="a minimum size in GB an object needs to be to be deleted",
        default=0.1,
        type=float
    )
    parser.add_argument(
        "-e", "--exclude_head",
        default=False,
        action='store_true',
        help="If present, no header will be produced in the output table."
    )
    return parser.parse_args()


def main():
    args = get_args()

    # load and filter relevant objects
    objects = multi_list_objects(args.s3_uri)
    objects = objects[objects.SIZE / 10**9 > args.min_size]
    objects = objects[objects.TIMESTAMP < datetime.now(pytz.utc) - timedelta(days=args.min_age)]
    objects = objects[~objects.URI.str.fullmatch(args.ignore)]
    if(args.ignorepath):
        ignores = pandas.read_csv(args.ignore, header=None)[0]
        objects = objects[~objects.URI.isin(set(ignores))]

    # identify set of files to delete
    keeps = objects.sort_values("TIMESTAMP").groupby(["CHECKSUM", "NAME"]).URI.first()
    deletes = pandas.DataFrame([
        {
            "URI": uri,
            "SIZE": s,
            "TIMESTAMP": t,
            "CHECKSUM": c,
            "OLDEST": keeps[(c, n)] 
        }
        for uri, s, t, c, n in zip(objects.URI, objects.SIZE, objects.TIMESTAMP, objects.CHECKSUM, objects.NAME)
        if(keeps[(c, n)] != uri)
    ])

    # handle deletion as needed
    if(len(deletes) == 0):
        sys.stderr.write(f"Nothing to Delete\n")
        return
    deletes.sort_values("CHECKSUM").to_csv(
        sys.stdout, sep="\t", index=None, header=not args.exclude_head
    )
    total = deletes.SIZE.sum() / 10 ** 9
    if(args.dryrun):
        sys.stderr.write(f"Total GB to delete : {total}\n")
    else:
        delete_s3_objects(deletes.URI)
        sys.stderr.write(f"Total GB deleted : {total}\n")


if __name__ == '__main__':
    main()
