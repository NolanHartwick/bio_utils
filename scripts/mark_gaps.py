#!/usr/bin/env python3
import argparse
import sys
from Bio import SeqIO
from io import StringIO

def find_gaps(seq, n):
    seq = seq.upper()
    gap_str = 'N' * n
    start = seq.find(gap_str)
    while start != -1:
        end = start
        while end < len(seq) and seq[end] == 'N':
            end += 1
        yield start, end
        start = seq.find(gap_str, end)

def process_fasta(fasta_file, n):
    results = []
    for record in SeqIO.parse(fasta_file, "fasta"):
        seq = str(record.seq)
        for start, end in find_gaps(seq, n):
            results.append(f"{record.id}\t{start}\t{end}")
    return results

def cli(args):
    parser = argparse.ArgumentParser(
        description="Finds gaps in FASTA file. Writes output to stdout"
    )
    parser.add_argument("fasta", help="Path to the FASTA file")
    parser.add_argument("n", type=int, help="Number of Ns in a row to consider as a gap")
    args = parser.parse_args(args)

    with open(args.fasta, "r") as fasta_file:
        results = process_fasta(fasta_file, args.n)
    for result in results:
        print(result)

def test():
    test_fasta = """>test
    ACGTNNNNACGTNNNNACGT
    """
    expected_results = [
        "test\t5\t8",
        "test\t13\t16"
    ]
    fasta_file = StringIO(test_fasta)
    result = process_fasta(fasta_file, 4)
    for r, e in zip(result, expected_results):
        assert r == e , f"Expected {e}, but got {r}"

if __name__ == "__main__":
    cli(sys.argv[1:])
    # test()
