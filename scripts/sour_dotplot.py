#!/usr/bin/env python3
import argparse
import sys

import pandas
from plotly import express

from Bio import SeqIO
from sourmash import MinHash


def get_args():
    parser = argparse.ArgumentParser(
        description="rapidly creates a dotplot like visual for two genomes",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "reference",
        type=str,
    )
    parser.add_argument(
        "other",
        type=str,
    )
    parser.add_argument(
        "-c", "--chunk_size",
        type=int,
        default=int(1e6),
        help="chunksize to use"
    )
    parser.add_argument(
        "-o", "--output",
        type=lambda v : sys.stdout if(v == "-") else open(v, "w"),
        default="sour_dots.html",
        help="An output location to write html to. Use - for stdout"
    )
    parser.add_argument(
        "-b", "--border",
        type=int,
        default=2
    )
    parser.add_argument(
        "-s", "--scaled",
        type=int,
        default=100
    )
    return parser.parse_args()


def break_into_chunks(seq, chunk_size):
    numchunks = len(seq) // chunk_size
    chunks = [int(len(seq) * i / numchunks) for  i in range(numchunks + 1)]
    return [seq[a:b] for a, b in zip(chunks[:-1], chunks[1:])]


def make_sourhash(seqs, scaled=100, ksize=31):
    mh = MinHash(n=0, ksize=ksize, scaled=scaled)
    for s in seqs:
        mh.add_sequence(s, force=True)
    return mh


def get_hashes(fasta, chunksize, scaled):
    ret = {}
    with open(fasta) as fin:
        for r in SeqIO.parse(fin, "fasta"):
            if(len(r) < chunksize):
                continue
            sys.stderr.write(f"Processing {r.id}\n")
            chunks = break_into_chunks(str(r.seq), chunksize)
            hashes = [make_sourhash([c], scaled) for c in chunks]
            ret[r.id] = hashes
    return ret


def compare(ref, alt, chunksize, scaled):
    refhashes = get_hashes(ref, chunksize, scaled)
    althashes = get_hashes(alt, chunksize, scaled)
    ret = pandas.DataFrame({
        (ctgref, fragref): {
            (ctgalt, fragalt): h1.similarity(h2)
            for ctgalt, alt_frags in althashes.items()
            for fragalt, h2 in enumerate(alt_frags)
        }
        for ctgref, ref_frags in refhashes.items()
        for fragref, h1 in enumerate(ref_frags)
    })
    return ret


def add_nulls(df, nullval, nullrows):
    rows = []
    idxes = []
    prevctg = None
    ridmid = pandas.Series(df.index.get_level_values(0)).value_counts() // 2
    for (rid, rpos), r in df.iterrows():
        if(rid != prevctg):
            prevctg = rid
            for i in range(nullrows):
                rows.append([nullval for _ in r])
                idxes.append(f"{rid}.blank{i}")
        rows.append(list(r))
        idxes.append(f"{rid}.frag{rpos}")
    ret = pandas.DataFrame(rows)
    ret.index = idxes
    ret.columns = df.columns
    return ret
        

def make_heatmap(df, nullrows):
    df = add_nulls(df, 0.5, nullrows)
    df = add_nulls(df.transpose(), 0.5, nullrows).transpose()
    return express.imshow(df, width=1980, height=1980)


def main():
    args = get_args()
    result = compare(args.reference, args.other, args.chunk_size, args.scaled)
    fig = make_heatmap(result, args.border)
    fig.write_html(args.output)


if(__name__ == "__main__"):
    main()
