#!/usr/bin/env python3
import argparse
import os
from itertools import combinations
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from sourmash import MinHash
from sourmash.signature import SourmashSignature
import pandas


def get_args():
    parser = argparse.ArgumentParser(
        description="Renames and reorients chromosomes in a set of fasta files",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "reference",
        type=str,
        help="an input fasta to be renamed. Will be used as reference for pairwise comparisons."
    )
    parser.add_argument(
        "others",
        type=str,
        nargs="*",
        help="other fasta files to be renamed"
    )
    parser.add_argument(
        "-l", "--length_cutoff",
        type=int,
        default=int(5e6),
        help="length cutoff to seperate chromosomes from contigs"
    )
    parser.add_argument(
        "-o", "--outdir",
        default="cleaned_fastas",
        help="a directory path to write fixed fastas too"
    )
    parser.add_argument(
        "--chr",
        type=str,
        default=["chr{}"],
        nargs="*",
        help="naming pattern for chromosomes. Keys: refid, id, num"
    )
    parser.add_argument(
        "--ctg",
        type=str,
        default=["ctg{}"],
        nargs="*",
        help="naming pattern(s) for non chromosomes"
    )
    parser.add_argument(
        "-i", "--include_original_ids",
        default=False,
        action='store_true',
        help="if present, original ids will be included as info in new contig headers"
    )
    return parser.parse_args()



def make_sourhash(seqs, scaled=100, ksize=31):
    mh = MinHash(n=0, ksize=ksize, scaled=scaled)
    for s in seqs:
        mh.add_sequence(s, force=True)
    return mh


def get_homologous_pairs(ref_hashes, other_hashes):
    """ ref_hashes : Dict<string: (hash1, hash2)>
        other_hashes: Dict<string: (hash1, hash2)>
        returns: Dict<string, (string, orient)>
    """
    similarities = pandas.DataFrame([
        {'REF': r1, 'REF_POS': i1, "OTHER": r2, "OTHER_POS": i2, "JAC": h1.similarity(h2)}
        for r1, p1 in ref_hashes.items()
        for r2, p2 in other_hashes.items()
        for i1, h1 in enumerate(p1)
        for i2, h2 in enumerate(p2)
    ])
    hits = similarities.sort_values('JAC').groupby('OTHER').last()
    hits['RC'] = hits.REF_POS != hits.OTHER_POS
    counts  = hits.REF.value_counts()
    badcounts = counts[counts > 1]
    if(len(badcounts) > 0):
        raise ValueError(f'Homologous Chromosomes do not form 121 pairs:\n{hits}')
    return {o: (r, rc) for o, r, rc in zip(hits.index, hits.REF, hits.RC)}
    


def get_hashes(path, cutoff):
    print(f"Computing Hashes for: {path}")
    hashes = {}
    with open(path) as fin:
        for r in SeqIO.parse(fin, "fasta"):
            if(len(r) > cutoff):
                print(r.id, len(r))
                seq = str(r.seq)
                hashes[r.id] = (
                    make_sourhash([seq[0:len(seq)//3]]),
                    make_sourhash([seq[2*len(seq)//3:]])
                )
    return hashes


def update_ref_fasta(inpath, outpath, chrom, ctg, cutoff, include_ids):
    """ reads inpath, sort by length, update ids based on chrom and ctg patterns

        returns a list of pairs mapping inpath ids to outpath ids in output order
    """
    seqs = []
    with open(inpath) as fin:
        for r in SeqIO.parse(fin, "fasta"):
            seqs.append(r)
    seqs = sorted(seqs, key=lambda r: len(r) * -1)
    name_pairs = []
    for i, r in enumerate(seqs):
        newid = chrom.format(i+1) if(len(r) > cutoff) else ctg.format(i+1)
        name_pairs.append((r.id, newid))
        r.id = newid
        if(not include_ids):
            r.name = ""
            r.description = ""
    with open(outpath, "w") as fout:
        SeqIO.write(seqs, fout, "fasta")
    return name_pairs


def update_other_fasta(inpath, outpath, chrom, ctg, cutoff, namemap, include_ids):
    """ namemap: {old_id: (new_number, orientation)]
    """
    seqs = []
    with open(inpath) as fin:
        for r in SeqIO.parse(fin, "fasta"):
            seqs.append(r)
    homologs = [r for r in seqs if(r.id in namemap)]
    homologs = sorted(homologs, key=lambda r: namemap[r.id][0])
    homologs = [
        r.reverse_complement(id=True, name=True, description=True)
        if(namemap[r.id][1]) else
        r
        for r in homologs
    ]
    nonhomologs = [r for r in seqs if(r.id not in namemap)]
    nonhomologs = sorted(nonhomologs, key=lambda r: len(r) * -1)
    name_pairs = []
    seqs = homologs + nonhomologs
    for i, r in enumerate(seqs):
        if(r.id in namemap):
            assert(namemap[r.id][0] == i)
        newid = chrom.format(i+1) if(len(r) > cutoff) else ctg.format(i+1)
        name_pairs.append((r.id, newid))
        r.id = newid
        if(not include_ids):
            r.name = ""
            r.description = ""
    with open(outpath, "w") as fout:
        SeqIO.write(seqs, fout, "fasta")
    return name_pairs


def sync_fasta_files(fastas, cutoff, chrom_patterns, ctg_patterns, outdir, include_ids):
    ref, *others = fastas
    ref_hashes = get_hashes(ref, cutoff)
    name_maps = []
    for f in others:
        other_hashes = get_hashes(f, cutoff)
        name_maps.append(get_homologous_pairs(ref_hashes, other_hashes))
    os.makedirs(outdir, exist_ok=True)
    ref_order = update_ref_fasta(
        ref,
        os.path.join(outdir, os.path.basename(ref)),
        chrom_patterns[0],
        ctg_patterns[0],
        cutoff,
        include_ids
    )
    ref_order_map = {old_id: i for i, (old_id, _) in enumerate(ref_order)}
    name_pairs = [ref_order]
    for other, name_map, chrom, ctg in zip(
        others, name_maps, chrom_patterns[1:], ctg_patterns[1:]
    ):
        name_map = {
            curid: (ref_order_map[refid], orient)
            for curid, (refid, orient) in name_map.items()
        }
        name_pair = update_other_fasta(
            other,
            os.path.join(outdir, os.path.basename(other)),
            chrom,
            ctg,
            cutoff,
            name_map,
            include_ids
        )
        name_pairs.append(name_pair)
    df = pandas.DataFrame([
        {'Assembly': assembly, "OldID": oid, "NewID": nid}
        for assembly, pairs in zip(fastas, name_pairs)
        for oid, nid in pairs
    ])
    df.to_csv(os.path.join(outdir, "renames.tsv"), sep="\t", index=None)


def main():
    args = get_args()
    fastas = [args.reference] + args.others
    assert(len(args.chr) == 1 or len(args.chr) == len(fastas))
    assert(len(args.ctg) == 1 or len(args.ctg) == len(fastas))
    chr_patterns = [args.chr[0]] * len(fastas) if(len(args.chr) == 1) else args.chr
    ctg_patterns = [args.ctg[0]] * len(fastas) if(len(args.ctg) == 1) else args.ctg
    sync_fasta_files(fastas, args.length_cutoff, chr_patterns, ctg_patterns, args.outdir, args.include_original_ids)


if __name__ == "__main__":
    main()
