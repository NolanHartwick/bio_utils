#!/usr/bin/env python3

import argparse
import sys
from collections import deque

from Bio import SeqIO
from BCBio import GFF
from Bio.SeqFeature import SeqFeature, CompoundLocation



def get_features(rec):
    """return a dict mapping feature ids to a list of associated features
    """
    collected = {}
    todo = deque(rec.features)
    while len(todo) > 0:
        cur = todo.pop()
        temp = collected.get(cur.id, [])
        temp.append(cur)
        collected[cur.id] = temp
        for sf in getattr(cur, 'sub_features', []):
            todo.append(sf)
    return collected


def mf_to_joins(featlist):
    """ Converts a list of features representing a gff3 style
        multiline feature into a single feature with multiple
        ranges.
    """
    # Extract the type of feature from the first feature in the list
    feature_type = featlist[0].type
    # Create a compound location from all feature locations
    joined_location = CompoundLocation([feat.location for feat in featlist])

    # Combine qualifiers from all features, prioritizing those from the first feature in the list
    combined_qualifiers = {}
    for feat in featlist:
        for key, value in feat.qualifiers.items():
            if key not in combined_qualifiers:
                combined_qualifiers[key] = value
            else:
                # Merge unique items only to avoid duplicates
                combined_qualifiers[key] = list(set(combined_qualifiers[key] + value))

    # Create a new feature with the combined location and qualifiers
    new_feature = SeqFeature(location=joined_location, type=feature_type, qualifiers=combined_qualifiers)
    return new_feature


def load_data(fasta_path, gff3_path):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    with open(gff3_path) as fin:
        return list(GFF.parse(fin, base_dict=seq_dict))


def gff_to_genbank(gff_path, fasta_path, outfile):
    """
    Convert FASTA and GFF3 files to a GenBank file.
    
    Parameters:
        gff_path (str): Path to the GFF3 file.
        fasta_path (str): Path to the FASTA file.
        outfile : writable file object where the GenBank file will be written.
    """
    data = load_data(fasta_path, gff_path)
    for record in data:
        record.annotations["molecule_type"] = "DNA"
        record.features = [
            fl[0] if (len(fl) == 1) else mf_to_joins(fl)
            for id, fl in get_features(record).items()
        ]
    # Step 3: Write to a GenBank file
    SeqIO.write(data, outfile, "genbank")


def get_args():
    """
    Parse command-line arguments.
    """
    parser = argparse.ArgumentParser(description="Convert GFF3 and FASTA files to GenBank format.")
    parser.add_argument("-g", "--gff", required=True, help="Path to the GFF3 file.")
    parser.add_argument("-f", "--fasta", required=True, help="Path to the FASTA file.")
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    gff_to_genbank(args.gff, args.fasta, sys.stdout)
