#!/usr/bin/env python3

import argparse
import subprocess
import os
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def check_dependencies():
    try:
        subprocess.run(["meryl", "--help"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except FileNotFoundError:
        print("Error: 'meryl' is not installed or not in the system PATH.")
        exit(1)



def run_command(command):
    print(f"Running: {' '.join(command)}")
    subprocess.run(command, check=True)


def count_kmers(dataset_files, output_dir, db_name):
    output_path = os.path.join(output_dir, db_name)
    run_command(["meryl", "count", "k=21", "output", output_path] + dataset_files)
    return output_path


def get_subset(db_paths, output_dir, subset, union=False):
    """
    Generate a k-mer subset based on the given subset specification.

    Args:
        db_paths (list): Paths to the meryl databases.
        output_dir (str): Directory to store the output meryl database.
        subset (list): A list of 0s and 1s indicating which databases to include in the subset.
        union (bool): Whether to use union-sum instead of intersect-sum.

    Returns:
        str: Path to the output meryl database containing the requested subset.
    """
    assert all(s in [0, 1] for s in subset), "Subset must contain only 0s and 1s"
    join_cmd = "union-sum" if union else "intersect-sum"
    output_path = os.path.join(output_dir, f"subset.{''.join(map(str, subset))}")
    left = [d for f, d in zip(subset, db_paths) if f]
    right = [d for f, d in zip(subset, db_paths) if not f]

    if len(right) == 0 and len(left) > 1:
        run_command(["meryl", "output", output_path, join_cmd] + left)
    elif len(left) == 1 and len(right) > 0:
        run_command(["meryl", "output", output_path, "difference", left[0]] + right)
    elif len(left) > 1 and len(right) > 0:
        run_command(["meryl", "output", output_path, "difference", "[", join_cmd] + left + ["]"] + right)
    else:
        raise ValueError(f"Requested set can't be created: {''.join(map(str, subset))}")

    return output_path


def generate_histogram(db_path, output_file):
    with open(output_file, 'w') as f:
        subprocess.run(["meryl", "histogram", db_path], stdout=f, check=True)


def merge_histograms(hist_files, subsets):
    histograms = {}
    for i, hist_file in enumerate(hist_files):
        df = pd.read_csv(hist_file, sep='\t', header=None, names=['Depth', subsets[i]])
        histograms[subsets[i]] = df.set_index('Depth')[subsets[i]].to_dict()

    merged_df = pd.DataFrame(histograms).fillna(0).astype(int)
    return merged_df


def plot_stacked_histogram(df, output_file, bins=50, xmax=None):
    cum_sum = df.iloc[:, 1:].sum(axis=1).cumsum()
    perc_sum = cum_sum / cum_sum.max()
    cutoff = perc_sum[perc_sum > 0.99].index[0] * 1.5 if xmax is None else xmax
    subset = df[df.index <= cutoff].stack().reset_index()
    subset.columns = ["Depth", "Set", "Count"]
    subset["Bases"] = subset["Count"] * subset["Depth"]

    sns.displot(data=subset, x="Depth", weights="Bases", hue="Set", multiple="stack", bins=bins)
    plt.title("Stacked K-mer Depth Histogram")
    plt.tight_layout()
    plt.savefig(output_file)
    plt.close()



def main():
    parser = argparse.ArgumentParser(description=(
        "Tool for characterizing and plottings kmer_spectra subsets given several datasets"
    ))
    subparsers = parser.add_subparsers()
    add_args_all(subparsers)
    add_args_count(subparsers)
    add_args_plot(subparsers)
    args = parser.parse_args()
    if(hasattr(args, "function")):
        args.function(args)
    else:
        parser.print_help()


def main_count(args):
    check_dependencies()
    if(len(args.dataset) == 1):
        raise ValueError("more than 1 dataset must be provided")
    if(not all(len(args.dataset) == len(s) for s in args.subset)):
        raise ValueError("subsets must have the same length as datasets")
    os.makedirs(args.output_dir, exist_ok=True)

    # Step 1: Count k-mers for each dataset
    db_paths = []
    for i, dataset_files in enumerate(args.dataset):
        db_name = f"meryl_db_{i+1}"
        db_paths.append(count_kmers(dataset_files, args.output_dir, db_name))

    # Step 2: Construct each specified subset
    subset_paths = []
    for subset_str in args.subset:
        subset = [int(c) for c in subset_str]
        subset_path = get_subset(db_paths, args.output_dir, subset)
        subset_paths.append(subset_path)

    # Step 3: Construct the remainder subset (k-mers not in any specified subset)
    remainder_path = get_subset(
        db_paths + subset_paths,
        args.output_dir,
        [1] * len(db_paths) + [0] * len(subset_paths),
        union=True
    )

    # Step 4: Generate histograms
    hist_files = []
    for subset_path, subset_str in zip(subset_paths, args.subset):
        hist_file = os.path.join(args.output_dir, f"histogram_{subset_str}.txt")
        generate_histogram(subset_path, hist_file)
        hist_files.append(hist_file)

    remainder_hist_file = os.path.join(args.output_dir, "histogram_remainder.txt")
    generate_histogram(remainder_path, remainder_hist_file)
    hist_files.append(remainder_hist_file)

    # Step 5: Merge histograms and save output
    merged_df = merge_histograms(hist_files, args.subset + ["remainder"])
    output_file = os.path.join(args.output_dir, "final_histogram.tsv")
    merged_df.to_csv(output_file, sep='\t')
    print(f"Histograms have been generated and combined in {output_file}")
    return output_file


def add_args_count(subparsers):
    sub = subparsers.add_parser("count", help="Quantifies kmers based on specified subsets")
    sub.add_argument('-o', "--output_dir", required=True, help="Output directory.")
    sub.add_argument("-d", "--dataset", action='append', nargs='+', required=True, help="Specify datasets.")
    sub.add_argument("-s", "--subset", action='append', required=True, help="Specify subsets as binary strings (e.g., '111', '010').")
    sub.set_defaults(function=main_count)


def main_plot(args):
    df = pd.read_csv(args.input_tsv, sep='\t', index_col=0)
    plot_stacked_histogram(df, args.output_plot, bins=args.bins, xmax=args.xmax)
    print(f"Plot generated and saved to {args.output_plot}")


def add_args_plot(subparsers):
    sub = subparsers.add_parser("plot", help="plots kmers based on provided 'count' results")
    sub.add_argument('input_tsv', help="Input TSV file with merged histogram.")
    sub.add_argument('output_plot', help="Output plot file.")
    sub.add_argument('--bins', type=int, default=50, help="Number of bins for the histogram.")
    sub.add_argument('--xmax', type=int, help="Maximum x-axis value (depth cutoff).")
    sub.set_defaults(function=main_plot)


def main_all(args):
    if(args.subset is None):
        temp = "1" + "0" * (len(args.dataset) - 1)
        args.subset = [temp[i:] + temp[:i] for i in range(len(args.dataset))]
        args.subset.append("1" * len(args.dataset))
    args.input_tsv = main_count(args)
    args.output_plot = args.plot_path
    main_plot(args)


def add_args_all(subparsers):
    sub = subparsers.add_parser("all", help="Quantifies kmers based on specified subsets and plots the spectra")
    sub.add_argument("-d", "--dataset", action='append', nargs='+', required=True, help="Specify datasets.")
    sub.add_argument('-o', "--output_dir", default="meryl_comparison_results", help="Output directory.")
    sub.add_argument('-p', '--plot_path', default="meryl_comparison_results/kmer.spectra.png", help="path to generated figure")
    sub.add_argument("-s", "--subset", action='append', help="Specify subsets as binary strings (e.g., '111', '010').")
    sub.add_argument('--bins', type=int, default=50, help="Number of bins for the histogram.")
    sub.add_argument('--xmax', type=int, help="Maximum x-axis value (depth cutoff).")
    sub.set_defaults(function=main_all)


if __name__ == "__main__":
    main()
