#!/usr/bin/env python3

import argparse
import pandas
from Bio import SeqIO
import sys


def load_fasta(fasta_path):
    with open(fasta_path) as fin:
        return list(SeqIO.parse(fin, "fasta"))


def load_gff(gffpath):
    ret = pandas.read_csv(
        gffpath,
        sep="\t",
        header=None,
        names=["SEQID", "SOURCE", "TYPE", "START", "END", "SCORE", "STRAND", "PHASE", "ATTR"]
    )
    ret["ID"] = ret.ATTR.str.extract("ID=(.*?)(;|$)")[0]
    return ret


def cli_main():
    parser = argparse.ArgumentParser(
        description=(
            "generates appropriate genespace bed given a protein fasta and a matching gff3"
        )
    )
    parser.add_argument(
        "fasta",
        help="fasta file matching the gff3 containing proteins"
    )
    parser.add_argument(
        "gff3",
        help="gff3 containing features to use"
    )
    parser.add_argument(
        "--features", 
        help="feature types to check against protein ids",
        default=["gene", "mRNA"],
        nargs="+"
    )
    args = parser.parse_args()
    return args


def main():
    args = cli_main()
    prots = load_fasta(args.fasta)
    gff = load_gff(args.gff3)
    fid_to_locs = {
        fid: (ctg, s, e)
        for fid, ctg, s, e, t in zip(gff.ID, gff.SEQID, gff.START, gff.END, gff.TYPE)
        if(t in args.features)
    }
    for p in prots:
        if(p.id in fid_to_locs):
            ctg, s, e = fid_to_locs[p.id]
            sys.stdout.write(f"{ctg}\t{int(s)}\t{int(e)}\t{p.id}\n")
        else:
            sys.stderr.write(f"WARNING: {p.id} not found in gff")



if(__name__ == "__main__"):
    main()
