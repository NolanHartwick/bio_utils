#!/usr/bin/env python3

from typing import TextIO, List, Iterable, Tuple
from collections import Counter
import argparse
import sys
import random
import json
import gzip


FastqIterable = Iterable[Tuple[str, str, str, str]]


def parse_fastq(
    f: TextIO,
    subset: List[int]=None
) -> FastqIterable:
    """ given a readable file like object with fastq data, iterate over its
        entries. Yielded tuples will have new lines if present. If subset is
        provided, yield entries as defined by subset

        input:
            f -
                A readable file like object containing fastq data
            subset -
                A list of integers defining a subset operation on the input f.
                entry[k] from fastq source f will be yielded subset[k] times

        >>> import io
        >>> fastq = io.StringIO("@read_1\\nAAACATTAG\\n+\\n:753;,:1.")
        >>> ret = next(parse_fastq(fastq))
        >>> ret[0]
        '@read_1\\n'
        >>> ret[1]
        'AAACATTAG\\n'
        >>> ret[2]
        '+\\n'
        >>> ret[3]
        ':753;,:1.'
        >>> subset = [1,1,0,2,0]
        >>> fin = open("example.fastq")
        >>> fastq_data = list(parse_fastq(fin, subset))
        >>> fin.close()
        >>> [len(e[1]) for e in fastq_data]
        [1363, 13507, 8714, 8714]
    """
    grouped = zip(f, f, f, f)
    for i, entry in enumerate(grouped):
        if(subset is None):
            yield entry
        else:
            for _ in range(subset[i]):
                yield entry


def write_fastq(
    fastq_data: FastqIterable,
    out: TextIO=sys.stdout
) -> None:
    """ Write fastq data to out.


        >>> import io
        >>> fastq = io.StringIO("@read_1\\nAAACA\\n+\\n:753;")
        >>> fastq_data = parse_fastq(fastq, [2])
        >>> outfile = io.StringIO()
        >>> write_fastq(fastq_data, outfile)
        >>> _ = outfile.seek(0)
        >>> outfile.read()
        '@read_1\\nAAACA\\n+\\n:753;\\n@read_1\\nAAACA\\n+\\n:753;\\n'
    """
    for entry in fastq_data:
        for line in entry:
            out.write(line)
        if(not line.endswith("\n")):
            out.write("\n")


class Psuedo_Text():
    """ A dummy class used by psuedo_fastq_data to allow for repeatedly taking
        samples from a single fastq file without requiring repeatedly reading
        file or storing the entire file in memory.
    """

    def __init__(self, str):
        self.size = len(str)

    def __len__(self):
        return self.size


def psuedo_fastq_data(
    fq_data: FastqIterable
) -> List[Tuple[None, Psuedo_Text, None, None]]:
    """ Utility function that will create a psuedo FastqIterable object
        that can be passed to subset fastq but doesn't actually store the
        fastq in memory. Essentially, it returns an object that will let
        subset fastq get the length information it needs. This is useful
        when you want to make many subsets of a single fastq file as it
        eliminates the need to repeatedly read the input fastq file

        >>> fq = parse_fastq(open("example.fastq"))
        >>> psuedo_fq = psuedo_fastq_data(fq)
        >>> random.seed(11)
        >>> subset_fastq(psuedo_fq, 0, 3)
        [0, 1, 0, 1, 1]
        >>> subset_fastq(psuedo_fq, 0, 1)
        [0, 0, 0, 0, 1]
    """
    return [(None, Psuedo_Text(s), None, None) for _, s, _, _ in fq_data]


def subset_fastq(
    fastq_data: FastqIterable,
    minlen: int,
    minreads: int,
    with_replace: bool=False,
    longest: bool=False
) -> List[int]:
    """ compute a random subset of provided fastq data such that at least
        minreads is in the subset and their total length is at least minlen.

        input-
            fastq_data:
                Some fastq data as returned by parse_fastq
            minlen:
                A minimum length. random reads will be selected from f until
                minlen bases worth of reads have been selected.
            with_replace:
                if true, allow for randomly selecting the same read multiple
                times. This flag can not be used with longest
            longest:
                if true return a subset containing the longest N reads such
                that N is as small as posible while the total length of reads
                exceeds minlen. If true, this will overrule the behavior of
                with_replace.

        returns: List[int]
            A list of integers defining a subset operation on the input f.
            entry[k] from fastq source f will be yielded subset[k] times

        >>> fq = list(parse_fastq(open("example.fastq")))
        >>> random.seed(0)
        >>> subset_fastq(fq, 0, 3)
        [1, 0, 0, 1, 1]
        >>> random.seed(11)
        >>> subset_fastq(fq, 0, 3)
        [0, 1, 0, 1, 1]
        >>> subset_fastq(fq, 0, 2, longest=True)
        [0, 1, 0, 0, 1]
        >>> subset_fastq(fq, 0, 6, with_replace=True)
        [0, 1, 0, 2, 3]
        >>> subset_fastq(fq, 5000, 0)
        [1, 0, 0, 0, 1]

    """
    lengths = [len(e[1]) - 1 for e in fastq_data]
    tot_reads = len(lengths)
    read_idx = range(tot_reads)
    len_sorted = list(sorted(read_idx, reverse=True, key=lengths.__getitem__))
    random_sorted = random.sample(read_idx, tot_reads)

    if(not with_replace):
        if(sum(lengths) <= minlen):
            raise ValueError("Requested length exceeds length of provided reads")
        if(len(lengths) <= minreads):
            raise ValueError("Requested number of reads exceeds number of provided reads")

    subset = []
    nreads = 0
    curlen = 0
    while(nreads < minreads or curlen < minlen):
        if(longest):
            selected_read = len_sorted[nreads]
        elif(with_replace):
            selected_read = random.choice(read_idx)
        else:
            selected_read = random_sorted[nreads]
        subset.append(selected_read)
        curlen += lengths[selected_read]
        nreads += 1

    counts = Counter(subset)
    return [counts[i] for i in read_idx]


def gzopen(fpath: str) -> TextIO:
    """ Open and return the file found at fpath. Unzip it if it is gzipped
    """
    if(fpath.endswith(".gz")):
        return gzip.open(fpath, "rt")
    else:
        return open(fpath)


def suffix_int(s: str) -> int:
    """ take suffix into account when converting s to int. suffixes are case
        insensitive. supported suffixes are. Deciminal portion is clipped
        after applying the suffix multiplier

        Suffix : Multiplier
        k : 10 ** 3
        m : 10 ** 6
        g : 10 ** 9
        t : 10 ** 12
        p : 10 ** 15

        >>> suffix_int("10k")
        10000
        >>> suffix_int("5K")
        5000
        >>> suffix_int("152m")
        152000000
        >>> suffix_int("2.3G")
        2300000000
        >>> suffix_int("4")
        4
    """
    mulipliers = {
        "k": 10**3, "K": 10**3,
        "m": 10**6, "M": 10**6,
        "g": 10**9, "G": 10**9,
        "t": 10**12, "T": 10**12,
        "p": 10**15, "P": 10**15
    }
    if(s[-1] in mulipliers):
        return int(mulipliers[s[-1]] * float(s[:-1]))
    else:
        return int(float(s))


def get_args() -> argparse.Namespace:
    """ Parse arguments for the cli version of subset_fastq
    """
    parser = argparse.ArgumentParser(
        description=(
            'This program will subset reads from a fastq (.gz) file. '
        )
    )
    parser.add_argument(
        "--minreads", "-r",
        type=suffix_int,
        help="A minimum number of reads to subset down to. Defaults to 0."
    )
    parser.add_argument(
        "--minlen", "-l",
        type=suffix_int,
        help="a minimum length of output reads to subset down to. Defaults to 0."
    )
    parser.add_argument(
        "--json_subset", "-j",
        help=(
            "if --minreads or --minlen are provided, the computed subset will "
            "be written to the path JSON_SUBSET. Else, read the json file at "
            "JSON_SUBSET and use that subset definiton to subset the provided "
            "fastq"
        )
    )
    parser.add_argument(
        "--seed", "-s",
        help="If provided, sets the RNG seed"
    )
    parser.add_argument(
        "--with_replace", "-w",
        action="store_true",
        help="Whether or not to sample with replacement"
    )
    parser.add_argument(
        "--keep_longest", "-k",
        action="store_true",
        help=(
            "Subset down to a longest set of reads instead of subsetting "
            "randomly. If present, this flag overrules --with_replace."
        )
    )
    parser.add_argument(
        "fastq",
        help="the path to a fastq file that needs to be processed"
    )
    args = parser.parse_args()
    return args


def main() -> None:
    """ impliments a CLI that wraps subset_fastq and related functions
    """
    args = get_args()
    if(all(x is None for x in [args.json_subset, args.minlen, args.minreads])):
        print(
            "ERROR : At least one of --json_subset, --minlen, or --minreads "
            "must be specified\n"
        )
        return
    if(args.seed is not None):
        random.seed(args.seed)
    if(args.minlen is None and args.minreads is None):
        with open(args.json_subset) as fin:
            subset = json.load(fin)
    else:
        with open(args.fastq) as fin:
            subset = subset_fastq(
                parse_fastq(fin),
                0 if args.minlen is None else args.minlen,
                0 if args.minreads is None else args.minreads,
                args.with_replace,
                args.keep_longest
            )
        if(args.json_subset is not None):
            with open(args.json_subset, "w") as fout:
                json.dump(subset, fout)
    with gzopen(args.fastq) as fin:
        write_fastq(parse_fastq(fin, subset))


if(__name__ == "__main__"):
    main()
    # import doctest
    # doctest.testmod()
