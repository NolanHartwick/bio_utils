#!/usr/bin/env python3

import argparse
import sys


def parse_fasta(f):
    """ Given a readable file like object containing fasta data, iterate
        over the fasta yielding a header, sequence pair for each entry

        >>> from io import StringIO
        >>> i = StringIO('>chr1\\nAAAAA\\n>chr2\\nCCCCC')
        >>> list(parse_fasta(i))
        [('chr1', 'AAAAA'), ('chr2', 'CCCCC')]
    """
    header = f.readline()[1:-1]
    if(header != ''):
        body = []
        for line in f:
            if(line[0] == '>'):
                yield (header, ''.join(body))
                header = line[1:-1]
                body = []
            else:
                body.append(line.rstrip())
        yield (header, ''.join(body))


if(__name__=="__main__"):
    parser = argparse.ArgumentParser(
        description=(
            "a tool that reports the length of each entry in a fasta file "
        )
    )
    parser.add_argument(
        "fasta",
        type=lambda s: sys.stdin if(s == "-") else open(s),
        help="A fasta file for which you want stats, use - for stdin"
    )
    args = parser.parse_args()
    for header, seq in parse_fasta(args.fasta):
        chrom = header.split(" ")[0]
        print(f"{chrom}\t{len(seq)}")
