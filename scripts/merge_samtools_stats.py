#!/usr/bin/env python3

import argparse
import sys
import pandas


def parse_samtools_stats(f):
    """ reads a samtools stats output file and return SN stats as a dictionary
    """
    with open(f) as fin:
        SN_lines = [l.split('\t') for l in fin if(l.startswith("SN"))]
        ret = {e[1][:-1]: float(e[2]) for e in SN_lines}
    return ret


def cli():
    parser = argparse.ArgumentParser(
        description="a tool that merges SN stats from multiple 'samtools stats' into a single table"
    )
    parser.add_argument(
        "bamstats",
        nargs="+",
        help="a set of samtools stats outputs"
    )
    return parser.parse_args()


def main():
    stats_paths = cli().bamstats
    all_data = pandas.DataFrame({f: parse_samtools_stats(f) for f in stats_paths})
    all_data.transpose().to_csv(sys.stdout, sep="\t")


if(__name__ == "__main__"):
    main()


