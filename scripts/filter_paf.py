#!/usr/bin/env python3

import argparse
import sys


def get_args():
    """ Parse arguments for the cli version of subset_fastq
    """
    parser = argparse.ArgumentParser(
        description=('filter a paf file by alignment length')
    )
    parser.add_argument(
        "paf",
        type=lambda s: sys.stdin if(s=="-") else open(s),
        help="path to paf file"
    )
    parser.add_argument(
        "-m", "--minlen",
        type=int,
        default=10000,
        help="block length filter threshold"
    )
    return parser.parse_args()


def main():
    args = get_args()
    for line in args.paf:
        fields = line.split("\t")
        if(int(fields[10]) >= args.minlen):
            sys.stdout.write(line)


if(__name__ == "__main__"):
    main()
