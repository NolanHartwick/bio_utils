#!/usr/bin/env python3


import glob
from collections import deque, defaultdict
import json
import argparse

import pandas
from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation


def get_seq(record, feature):
    ret = record.seq[feature.location.nofuzzy_start: feature.location.nofuzzy_end]
    if(feature.strand == -1):
        ret = ret.reverse_complement()
    return str(ret)


def load_data(fasta_path, gff3_path):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    with open(gff3_path) as fin:
        ret = list(GFF.parse(fin, base_dict=seq_dict))
        return {c.id: c for c in ret}


def add_source_to_mrna(records, fpath, lang):
    for ctg in records:
        mrnas = get_features(records[ctg].features, lang['mRNA'])
        for m in mrnas:
            m.qualifiers["file_source"] = fpath

    
def get_features(features, feat_type):
    toprocess = deque(features)
    genes = []
    while(len(toprocess) > 0):
        next_f = toprocess.popleft()
        if(next_f.type == feat_type):
            genes.append(next_f)
        else:
            for f in next_f.sub_features:
                toprocess.append(f)
    return genes


def group_genes(genes):
    groups = []
    cur = None
    cur_end = -1
    for f in genes:
        if(f.location.nofuzzy_start > cur_end):
            if(cur is not None):
                groups.append(cur)
            cur = [f]
            cur_end = f.location.nofuzzy_end
        else:
            cur.append(f)
            cur_end = max(f.location.nofuzzy_end, cur_end)
    return groups


def merge_groups(groups, template, lang):
    ret = []
    content_map = {}
    name_map = defaultdict(lambda : [])
    for i, group in enumerate(groups):
        start = min(g.location.nofuzzy_start for g in group)
        end = max(g.location.nofuzzy_end for g in group)
        strand = group[0].strand
        refs = [g for g in group if(g.qualifiers['label'] == "ref")]
        if(len(refs) == 1):
            newID = refs[0].id
        else:
            newID = template.format(num=i + 1)
        content_map[newID] = [g.id for g in group]
        qual = {
            "source": "merged",
            "ID": newID,
            "component_genes": [g.id for g in group]
        } 
        new_gene = SeqFeature(
            FeatureLocation(start, end),
            type="gene",
            strand=strand,
            qualifiers=qual,
            id=newID
        )
        for i, g in enumerate(group):
            transcripts = [t for t in g.sub_features if(t.type == lang["mRNA"])]
            for j, t in enumerate(transcripts):
                t.qualifiers['old_parent'] = t.qualifiers['Parent']
                t.qualifiers['old_id'] = t.id
                t.qualifiers['Parent'] = []
                t.id = newID + f"_T{i}.{j:02d}"
                t.qualifiers['ID'] = t.id
                name_map[t.id].append(t.qualifiers["old_id"])
                exons = [f for f in t.sub_features if(f.type== lang['exon'])]
                cds = [f for f in t.sub_features if(f.type== lang['CDS'])]
                utr3 = [f for f in t.sub_features if(f.type== lang['utr3'])]
                utr5 = [f for f in t.sub_features if(f.type== lang['utr5'])]
                for k, e in enumerate(exons):
                    e.qualifiers['old_parent'] = e.qualifiers['Parent']
                    e.qualifiers['old_id'] = e.id
                    e.qualifiers['Parent'] = []
                    e.id = newID + f"_T{i}.{j:02d}.e{k:02d}"
                    e.qualifiers['ID'] = e.id
                    name_map[e.id].append(e.qualifiers["old_id"])
                for k, p in enumerate(cds):
                    p.qualifiers['old_parent'] = p.qualifiers['Parent']
                    p.qualifiers['old_id'] = p.id
                    p.qualifiers['Parent'] = []
                    p.id = newID + f"_P{i}.{j:02d}"
                    p.qualifiers['ID'] = p.id
                    name_map[p.id].append(p.qualifiers["old_id"])
                for k, u in enumerate(utr3):
                    u.qualifiers['old_parent'] = u.qualifiers['Parent']
                    u.qualifiers['old_id'] = u.id
                    u.qualifiers['Parent'] = []
                    u.id = newID + f"_T{i}.{j:02d}.3utr{k}"
                    u.qualifiers['ID'] = u.id
                    name_map[u.id].append(u.qualifiers["old_id"])
                for k, u in enumerate(utr5):
                    u.qualifiers['old_parent'] = u.qualifiers['Parent']
                    u.qualifiers['old_id'] = u.id
                    u.qualifiers['Parent'] = []
                    u.id = newID + f"_T{i}.{j:02d}.5utr{k}"
                    u.qualifiers['ID'] = u.id
                    name_map[u.id].append(u.qualifiers["old_id"])
        new_gene.sub_features = [s for f in group for s in f.sub_features]
        ret.append(new_gene)
    return ret, content_map, name_map


def merge_gff3s(ref, alts, gid_pattern, lang):
    """ merge gff3 records from alts into ref, generating new names when required 
    """
    all_ctgs = set(list(ref) + [ctg for a in alts for ctg in a])
    content_map = {}
    name_map = {}
    for ctg in all_ctgs:
        genes = [get_features(ref[ctg].features, lang['gene'])] + [
            get_features(a[ctg].features, lang['gene']) for a in alts
        ]
        labels = ["ref"] + ["alt"] * len(alts)
        for a, l in zip(genes, labels):
            for g in a:
                g.qualifiers['label'] = l
        genes = [g for a in genes for g in a]
        genes = list(sorted(genes, key=lambda g: g.location.nofuzzy_start))
        plus_groups = group_genes([g for g in genes if(g.strand == 1)])
        neg_groups = group_genes([g for g in genes if(g.strand == -1)])
        new_feats, cont_map, nm_map = merge_groups(
            plus_groups + neg_groups,
            gid_pattern.format(ctg=ctg),
            lang
        )
        for k, v in cont_map.items():
            content_map[k] = v
        for k, v in nm_map.items():
            name_map[k] = v
        ref[ctg].features = new_feats
    return ref, content_map, name_map



def cli_main():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that merges gene models from alt gff3s into a ref gff3."
        )
    )
    parser.add_argument(
        "-f", "--fasta",
        help="fasta file matching the gff3s"
    )
    parser.add_argument(
        "-r", "--ref",
        help="reference gff3"
    )
    parser.add_argument(
        "alts",
        nargs="*",
        help="gff3 containing gene models to merge in"
    )
    parser.add_argument(
        "-o", "--outprefix",
        default='merged',
        help="a prefix to use for generating output files. Defaults to 'merged' "
    )
    parser.add_argument(
        "-p", "--pattern",
        default="merged.{ctg}.{{num:05d}}",
        help=(
            "a python format string used for naming gene models when necessary. "
            "defaults to 'merged.{ctg}.{{num:05d}}'"
        )
    )
    parser.add_argument(
        "--gene_feature",
        default="gene",
        help="feature type to look for to identify gene features. Default is 'gene'"
    )
    parser.add_argument(
        "--cds_feature",
        default="CDS",
        help=(
            "feature type to look for to identify CDS features. Should be "
            "children of --mrna_feature. Default is 'CDS'"
        )
    )
    parser.add_argument(
        "--exon_feature",
        default="exon",
        help=(
            "feature type to look for to identify exon features. Should be "
            "children of --mrna_feature. Default is 'exon'"
        )
    )
    parser.add_argument(
        "--mrna_feature",
        default="mRNA",
        help=(
            "feature type to look for to identify transcript features. "
            "Should be children of --gene_feature. Default is 'mRNA'"
        )
    )
    parser.add_argument(
        "--utr3_feature",
        default="three_prime_UTR",
        help=(
            "feature type to look for to identify three prime UTR features. "
            "Should be children of --mrna_feature. Default is 'three_prime_UTR'"
        )
    )
    parser.add_argument(
        "--utr5_feature",
        default="five_prime_UTR",
        help=(
            "feature type to look for to identify five prime UTR features. "
            "Should be children of --mrna_feature. Default is 'five_prime_UTR'"
        )
    )
    args = parser.parse_args()
    lang = {
        "mRNA": args.mrna_feature,
        "gene": args.gene_feature,
        "exon": args.exon_feature,
        "CDS": args.cds_feature,
        "utr3": args.utr3_feature,
        "utr5": args.utr5_feature
    }
    merge_main(
        args.fasta,
        args.ref,
        args.alts,
        args.outprefix,
        args.pattern,
        lang
    )


def merge_main(fasta, ref, alts, out, pattern, lang):
    ref = load_data(fasta, ref)
    alt_gffs = [load_data(fasta, r) for r in alts]
    for a, r in zip(alt_gffs, alts):
        add_source_to_mrna(a, r, lang)
    ret, cont_map, name_map = merge_gff3s(ref, alt_gffs, pattern, lang)
    with open(out + ".gff3", "w") as fout:
        GFF.write(list(ret.values()), fout)
    with open(out + ".name_map.json", "w") as fout:
        json.dump(name_map, fout)
    with open(out + ".content.json", "w") as fout:
        json.dump(cont_map, fout)
    with open(out + ".genes.fasta", "w") as fout:
        for seq in ret.values():
            for gene in seq.features:
                gene_seq = get_seq(seq, gene)
                fout.write(f">{gene.id}\n{gene_seq}\n")


if(__name__ == "__main__"):
    cli_main()

