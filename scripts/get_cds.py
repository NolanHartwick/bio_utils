#!/usr/bin/env python3

import argparse
from collections import deque
import sys
from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord


def load_data(fasta_path, gff3_path):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    with open(gff3_path) as fin:
        return list(GFF.parse(fin, base_dict=seq_dict))


def get_children(f, ftype, include_self=True):
    """ Recursively move through sub_features of f and return a set of all
        sub_features matching ftype
    """
    if(include_self):
        to_process = deque([f])
    else:
        to_process = deque(f.sub_features)
    ret = {}
    processed = set()
    while(len(to_process) > 0):
        f = to_process.popleft()
        if(f.type == ftype):
            ret[f.id] = f
        processed.add(f.id)
        for s in f.sub_features:
            if(s.id not in processed):
                to_process.append(s)
    return ret


def get_feats(record, featuretype):
    """ Return a set of features matching featuretype associated with some seqRecord object
    """
    return {gid: g for f in record.features for gid, g in get_children(f, featuretype).items()}


def locmax(somelist):
    return max((v, i) for i, v in enumerate(somelist))


def get_gene_regions(data, uplength, downlength, genefeat, transfeat, cdsfeat):
    ret = []
    for rec in data:
        ctglen = len(rec)
        genes = get_feats(rec, featuretype=genefeat)
        for fid, f in genes.items():
            cds = [
                [c for c in t.sub_features if(c.type == cdsfeat)]
                for t in f.sub_features
                if(t.type == transfeat)
            ]
            if(len(cds) == 0):
                continue
            cdslens = [sum(c.location.nofuzzy_end - c.location.nofuzzy_start for c in l) for l in cds]
            _, pos = locmax(cdslens)
            best_trans = list(sorted(cds[pos], key=lambda f: f.location.nofuzzy_start))
            naive_seq = "".join(str(rec.seq[f.location.nofuzzy_start: f.location.nofuzzy_end]) for f in best_trans)
            before_seq = rec.seq[
                    max(0, best_trans[0].location.nofuzzy_start - downlength):best_trans[0].location.nofuzzy_start
            ]
            after_seq = rec.seq[
                best_trans[-1].location.nofuzzy_end:min(ctglen, best_trans[-1].location.nofuzzy_end + uplength)
            ]
            seq = Seq.Seq(str(before_seq) + naive_seq + str(after_seq))
            if(f.strand == -1):
                seq = seq.reverse_complement()
            ret.append(SeqRecord(seq, fid, "", ""))
    return ret


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that extracts transcripts from a fasta file based on a provided gff3."
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "fasta",
        help="fasta file matching the gff3"
    )
    parser.add_argument(
        "gff3",
        help="gff3 containing features to use"
    )
    parser.add_argument(
        "-o", "--output",
        default=sys.stdout,
        type=lambda s: open(s, "w") if s != "-" else sys.stdout,
        help="output path. Use '-' for stdout."
    )
    parser.add_argument(
        "-g", "--gene_feature",
        default="gene",
    )
    parser.add_argument(
        "-t", "--transcript_feature",
        default="mRNA",
    )
    parser.add_argument(
        "-c", "--cds_feature",
        default="CDS",
    )
    parser.add_argument(
        "-u", "--upstream",
        default=500,
        type=int,
        help="length upstream of feature to include in region"
    )
    parser.add_argument(
        "-d", "--downstream",
        default=500,
        type=int,
        help="length downstream of feature to include in region"
    )
    args = parser.parse_args()
    return args


def main():
    args = get_args()
    data = load_data(args.fasta, args.gff3)
    genes = get_gene_regions(
        data, args.upstream, args.downstream,
        args.gene_feature, args.transcript_feature, args.cds_feature
    )
    for g in genes:
        args.output.write(f">{g.id}\n{g.seq}\n")

if(__name__ == "__main__"):
    main()

