#!/usr/bin/env python3

import boto3
import argparse
from urllib.parse import urlparse

def list_objects(bucket_name, prefix):
    s3_client = boto3.client('s3')
    paginator = s3_client.get_paginator('list_objects_v2')

    for page in paginator.paginate(Bucket=bucket_name, Prefix=prefix):
        for obj in page.get('Contents', []):
            yield obj

def format_s3_uri(bucket, key):
    return f"s3://{bucket}/{key}"

def main():
    parser = argparse.ArgumentParser(description='List S3 objects with a specific prefix')
    parser.add_argument('s3_uri', help='S3 URI (e.g., s3://my-bucket/my-prefix)')
    args = parser.parse_args()

    parsed_uri = urlparse(args.s3_uri)
    bucket_name = parsed_uri.netloc
    prefix = parsed_uri.path.lstrip('/')

    for obj in list_objects(bucket_name, prefix):
        size = obj['Size']
        timestamp = obj['LastModified']
        s3_uri = format_s3_uri(bucket_name, obj['Key'])
        checksum = obj['ETag'].strip('"')

        print(f"{size}\t{timestamp}\t{s3_uri}\t{checksum}")

if __name__ == '__main__':
    main()
