#!/usr/bin/env python3

import argparse
import json
import sys
from collections import defaultdict, deque
from BCBio import GFF
from Bio import SeqIO

def parse_args():
    parser = argparse.ArgumentParser(
        description="Compare two GFF3 files, keep per-transcript CDS intervals, find overlapping genes, compute max Jaccard."
    )
    parser.add_argument("gff1", help="First GFF3 file")
    parser.add_argument("gff2", help="Second GFF3 file")
    parser.add_argument("-r", "--ref_fasta", help="Optional reference FASTA", default=None)
    parser.add_argument("-o", "--output_json", help="Output JSON path (default=stdout)", default="-")
    return parser.parse_args()

# ------------------------------------------------------------------------------
# Generic feature gathering
# ------------------------------------------------------------------------------

def gather_feats(features, feat_types):
    """
    Recursively collect features of any type in 'feat_types' from a list of starting features.
    We pop from 'stack', and if a feature's type is in 'feat_types', we add it to the result.
    Otherwise, we extend the stack with sub_features.
    """
    result = []
    stack = list(features)
    while stack:
        f = stack.pop()
        if f.type.lower() in feat_types:
            result.append(f)
        else:
            stack.extend(f.sub_features)
    return result

def gather_genes(seqrecord):
    """Return all 'gene' features in this SeqRecord."""
    return gather_feats(seqrecord.features, {"gene"})

def gather_transcripts(gene_feature):
    """Return all mRNA/transcript features under this gene."""
    return gather_feats(gene_feature.sub_features, {"mrna", "transcript"})

# ------------------------------------------------------------------------------
# Building gene data with per-transcript CDS
# ------------------------------------------------------------------------------

def collect_cds_intervals(transcript_feat):
    """
    Collect & merge all CDS intervals under a single transcript.
    Return a list of merged intervals [(start,end), ...], 1-based inclusive, no overlaps.
    """
    cds_feats = gather_feats(transcript_feat.sub_features, {"cds"})
    if not cds_feats:
        return []
    intervals = []
    for sf in cds_feats:
        start = sf.location.start + 1  # convert to 1-based inclusive
        end   = sf.location.end
        intervals.append((start, end))

    intervals.sort(key=lambda x: x[0])
    merged = []
    cur_s, cur_e = intervals[0]
    for i in range(1, len(intervals)):
        s, e = intervals[i]
        if s <= cur_e + 1:
            cur_e = max(cur_e, e)
        else:
            merged.append((cur_s, cur_e))
            cur_s, cur_e = s, e
    merged.append((cur_s, cur_e))
    return merged

def build_gene_list(seqrecords):
    """
    For each gene, store:
      {
        "gene_id": <str>,
        "seqid":   rec.id,
        "start":   <min among all transcripts' CDS>,
        "end":     <max among all transcripts' CDS>,
        "transcripts": [
          [ (s1,e1),(s2,e2),... ],  # merged CDS intervals for transcript 1
          [ ... ],                  # transcript 2
          ...
        ]
      }
    """
    genes_out = []
    for rec in seqrecords:
        gene_feats = gather_genes(rec)
        for gf in gene_feats:
            g_id  = gf.qualifiers.get("ID", [f"gene_{id(gf)}"])[0]
            tx_feats = gather_transcripts(gf)
            # if there's no mRNA/transcript level, treat the gene itself as transcript
            if not tx_feats:
                tx_feats = [gf]
            all_tx_intervals = []
            min_start, max_end = None, None
            for txf in tx_feats:
                cds_merged = collect_cds_intervals(txf)
                all_tx_intervals.append(cds_merged)
                if cds_merged:
                    if min_start is None or cds_merged[0][0] < min_start:
                        min_start = cds_merged[0][0]
                    if max_end is None or cds_merged[-1][1] > max_end:
                        max_end = cds_merged[-1][1]

            # If no CDS found in transcripts, fallback to gene's own bounding coords
            if min_start is None:
                min_start = gf.location.start + 1
                max_end   = gf.location.end

            genes_out.append({
                "gene_id":    g_id,
                "seqid":      rec.id,
                "start":      min_start,
                "end":        max_end,
                "transcripts": all_tx_intervals  # list of intervals-lists
            })
    return genes_out

# ------------------------------------------------------------------------------
# Overlap detection -> connected blocks
# ------------------------------------------------------------------------------

def intervals_overlap(s1,e1, s2,e2):
    return not (e1 < s2 or s1 > e2)

def find_blocks(genes1, genes2):
    """
    Bipartite adjacency by bounding intervals, then BFS for connected components -> blocks.
    """
    from collections import defaultdict, deque
    adjacency = defaultdict(list)
    for i, g1 in enumerate(genes1):
        for j, g2 in enumerate(genes2):
            if g1["seqid"] == g2["seqid"]:
                if intervals_overlap(g1["start"], g1["end"], g2["start"], g2["end"]):
                    adjacency[i].append(j)

    visited_i, visited_j = set(), set()
    blocks = []
    for i in range(len(genes1)):
        if i in visited_i:
            continue
        queue = deque([("i", i)])
        block_i, block_j = set(), set()
        while queue:
            typ, idx = queue.popleft()
            if typ == "i":
                if idx in visited_i:
                    continue
                visited_i.add(idx)
                block_i.add(idx)
                for j in adjacency[idx]:
                    if j not in visited_j:
                        queue.append(("j", j))
            else:  # "j"
                if idx in visited_j:
                    continue
                visited_j.add(idx)
                block_j.add(idx)
                for i2, lst in adjacency.items():
                    if idx in lst and i2 not in visited_i:
                        queue.append(("i", i2))

        if block_i or block_j:
            blocks.append({
                "genes1": [genes1[x]["gene_id"] for x in sorted(block_i)],
                "genes2": [genes2[x]["gene_id"] for x in sorted(block_j)]
            })
    return blocks

# ------------------------------------------------------------------------------
# Jaccard logic (max over transcript pairs)
# ------------------------------------------------------------------------------

def interval_length(ints):
    return sum((e - s + 1) for s,e in ints)

def interval_intersection_length(a, b):
    i, j = 0, 0
    inter = 0
    while i < len(a) and j < len(b):
        astart,aend = a[i]
        bstart,bend = b[j]
        ov_start = max(astart,bstart)
        ov_end   = min(aend,bend)
        if ov_start <= ov_end:
            inter += (ov_end - ov_start + 1)
        if aend < bend:
            i += 1
        else:
            j += 1
    return inter

def jaccard(txA, txB):
    if not txA and not txB:
        return 0.0
    inter_len = interval_intersection_length(txA, txB)
    union_len = interval_length(txA) + interval_length(txB) - inter_len
    return inter_len / union_len if union_len else 0.0

def gene_gene_max_jaccard(gA, gB):
    max_j = 0.0
    for txA in gA["transcripts"]:
        for txB in gB["transcripts"]:
            val = jaccard(txA, txB)
            if val > max_j:
                max_j = val
    return max_j

def add_jaccards(blocks, genes1, genes2):
    gdict1 = {g["gene_id"]: g for g in genes1}
    gdict2 = {g["gene_id"]: g for g in genes2}
    for blk in blocks:
        pi = {}
        for ga in blk["genes1"]:
            pi[ga] = {}
            for gb in blk["genes2"]:
                jj = gene_gene_max_jaccard(gdict1[ga], gdict2[gb]) * 100
                pi[ga][gb] = round(jj, 2)
        blk["percent_identities"] = pi

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------

def main():
    args = parse_args()

    # If you need the reference sequences for some reason:
    ref_dict = {}
    if args.ref_fasta:
        with open(args.ref_fasta) as rf:
            ref_dict = SeqIO.to_dict(SeqIO.parse(rf, "fasta"))

    with open(args.gff1) as fh1:
        seqrecords1 = list(GFF.parse(fh1, base_dict=ref_dict))
    with open(args.gff2) as fh2:
        seqrecords2 = list(GFF.parse(fh2, base_dict=ref_dict))

    # Build gene lists, preserving transcripts separately
    genes1 = build_gene_list(seqrecords1)
    genes2 = build_gene_list(seqrecords2)

    # Overlapping blocks
    blocks = find_blocks(genes1, genes2)

    # Add max transcript-level Jaccard
    add_jaccards(blocks, genes1, genes2)

    # Output
    out_handle = sys.stdout if args.output_json == "-" else open(args.output_json, "w")
    with out_handle:
        json.dump(blocks, out_handle, indent=2)

    print(f"[INFO] Wrote {len(blocks)} blocks to '{args.output_json}'" 
          if args.output_json != "-" else "[INFO] Wrote to stdout.")

if __name__ == "__main__":
    main()
