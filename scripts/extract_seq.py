#!/usr/bin/env python3

import argparse
import os
from collections import deque
import json
import pandas
from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord


def load_data(fasta_path, gff3_path):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    with open(gff3_path) as fin:
        return list(GFF.parse(fin, base_dict=seq_dict))


def get_seq(record, feature):
    return str(record.seq[feature.location.nofuzzy_start: feature.location.nofuzzy_end])


def sort_features(feats):
    return list(sorted(feats, key=lambda f: f.location.nofuzzy_start))


def get_children(f, ftype, include_self=True):
    """ Recursively move through sub_features of f and return a set of all
        sub_features matching ftype
    """
    if(include_self):
        to_process = deque([f])
    else:
        to_process = deque(f.sub_features)
    ret = []
    processed = set()
    while(len(to_process) > 0):
        f = to_process.popleft()
        if(f.type == ftype):
            ret.append(f)
        processed.add(f.id)
        for s in f.sub_features:
            if(s.id not in processed):
                to_process.append(s)
    return ret


def get_genes(record, keys):
    """ Return a set of genes associated with some seqRecord object
    """
    return {g.id: g for f in record.features for g in get_children(f, keys["gene"])}


def get_promoters(data, promoter_length, keys):
    prom_records = []
    for rec in data:
        ctglen = len(rec)
        genes = get_genes(rec, keys)
        for gid, g in genes.items():
            cds = get_children(g, keys['CDS'])
            if(len(cds) == 0):
                continue
            if(g.strand == -1):
                pos = max(f.location.nofuzzy_end for f in cds)
                seq = rec.seq[max(0, pos): min(pos + promoter_length, ctglen)]
                seq = seq.reverse_complement()
            else:
                pos = min(f.location.nofuzzy_start for f in cds)
                seq = rec.seq[max(0, pos - promoter_length) : min(pos, ctglen)]
            prom_records.append(SeqRecord(seq, g.id, "", ""))
    return prom_records


# UNUSED. Plan to extract genebody sequences but didn't
def get_seqs_for_feature(data, featuretype="gene"):
    records = []
    for rec in data:
        features = deque(f for f in rec.features)
        while len(features) > 0:
            f = features.popleft()
            features.extend(f.sub_features)
            if(f.type == featuretype):
                seq = Seq.Seq(get_seq(rec, f))
                if(f.strand == -1):
                    seq = seq.reverse_complement()
                seq = SeqRecord(seq, f.id, "", "")
                records.append(seq)
    return records


def primary_cds_and_prots(data, source, keys):
    """extracts the cds sequence associated with the longest transcript associated with each gene"""
    cds_records = []
    prot_records = []
    for rec in data:
        genes = get_genes(rec, keys)
        if(len(genes) == 0):
            continue
        df = pandas.DataFrame([
            {"gene": g, "transcript": t, "gid": g.id, "tid": t.id}
            for gid, g in genes.items()
            for t in get_children(g, keys["mRNA"])
        ])
        if(len(df) == 0):
            continue
        df["CDS"] = df.transcript.apply(lambda t: sort_features(get_children(t, keys["CDS"])))
        df = df[df.CDS.apply(len) != 0]
        df["strand"] = df.CDS.apply(lambda cdses: cdses[0].strand != -1)
        df["offset"] = [
            int(cdses[0 if strand else -1].qualifiers["phase"][0])
            for strand, cdses in zip(df.strand, df.CDS)
        ]
        df["niave_cds_seq"] = df.CDS.apply(lambda cdses: "".join(get_seq(rec, f) for f in cdses))
        df["cds_seq"] = [
            Seq.Seq(seq) if(strand) else Seq.Seq(seq).reverse_complement()
            for seq, strand in zip(df.niave_cds_seq, df.strand)
        ]
        df["cds_record"] = [SeqRecord(s, t.id, "", "") for t, s in zip(df.transcript, df.cds_seq)]
        df["cds_len"] = df.cds_seq.apply(len)
        df["transcript_len"] = df.transcript.apply(lambda t: t.location.nofuzzy_end - t.location.nofuzzy_start)
        df["prot"] = [
            SeqRecord(s[o:].translate().seq, s.id, "", "")
            for s, o in zip(df.cds_record, df.offset)
        ]
        df["tid_order"] = df.tid.rank() * -1
        if(source == "phytozome"):
            primary_df = df[df.transcript.apply(lambda t: t.qualifiers["longest"][0] == "1")]
        else:
            primary_df = df.groupby("gid").apply(
                lambda df: df.sort_values(["cds_len", "transcript_len", "tid_order"]).iloc[-1]
            )
        cds_records.extend(list(primary_df.cds_record))
        prot_records.extend(primary_df.prot)
    return cds_records, prot_records


def structure_map(data):
    r = {}
    for rec in data:
        d = deque([(f, None) for f in rec.features])
        while(len(d) > 0):
            f, p = d.popleft()
            r[f.id] = {'parent': p, 'type': f.type}
            for s in f.sub_features:
                if(s.id not in r):
                    d.append((s, f.id))
    return r            



def generate_all_files(fasta_path, gff3_path, outprefix, promoter_length, source, keys):
    """ generates all needed sequences for crops bioinformatics analysis

        input:
            fasta_path : path to fasta file containing genome
            gff3_path : path to gff3 file containing gene models
            outprefix : pathlike prefix where files should be written
            promoter_length : how big of a promoter region to extract
            source : phytozome or other
            keys : dict of keys for mapping to alternate gff3 files
    """
    data = load_data(fasta_path, gff3_path)
    cds, prots = primary_cds_and_prots(data, source, keys)
    SeqIO.write(cds, outprefix + '.cds.fasta', "fasta")
    SeqIO.write(prots, outprefix + '.proteins.fasta', "fasta")
    promoters = get_promoters(data, promoter_length, keys)
    SeqIO.write(promoters, outprefix + ".promoters.fasta", "fasta")
    genes = get_seqs_for_feature(data, keys['gene'])
    SeqIO.write(genes, outprefix + ".gene_regions.fasta", "fasta")
    name_map = structure_map(data)
    with open(outprefix + ".ids.json", "w") as fout:
        json.dump(name_map, fout, indent=4)


def cli_main():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that extracts biological sequences from a fasta file based on a provided gff3."
        )
    )
    parser.add_argument(
        "fasta",
        help="fasta file matching the gff3"
    )
    parser.add_argument(
        "gff3",
        help="gff3 containing features to use"
    )
    parser.add_argument(
        "-o", "--outprefix",
        default=None,
        help="a prefix to use for generating output files. Defaults to gff3 path after suffix trimming"
    )
    parser.add_argument(
        "--promoter_length",
        default=500,
        type=int,
        help="default length for promoter region to extract"
    )
    parser.add_argument(
        "--gene_feature",
        default="gene",
        help="feature type to look for to identify gene features. Default is 'gene'"
    )
    parser.add_argument(
        "--cds_feature",
        default="CDS",
        help="feature type to look for to identify CDS features. Should be children of --mran_feature. Default is 'CDS'"
    )
    parser.add_argument(
        "--mrna_feature",
        default="mRNA",
        help="feature type to look for to identify transcript features. Should be children of --gene_feature. Default is 'mRNA'"
    )
    parser.add_argument(
        "--source", "-s",
        choices=["phytozome", "other"],
        default="other",
        help="What format is the gff3 in"
    )
    args = parser.parse_args()
    if(args.outprefix is None):
        args.outprefix = os.path.splitext(args.gff3)[0]
    keys = {
        "mRNA": args.mrna_feature,
        "gene": args.gene_feature,
        "CDS": args.cds_feature
    }
    generate_all_files(
        args.fasta,
        args.gff3,
        args.outprefix,
        args.promoter_length,
        args.source,
        keys
    )



if(__name__ == "__main__"):
    # fasta_path = "/home/nol/BIO/workflows/testdata/crops_genomes/phyto_at/Athaliana_167_TAIR9.fa"
    # gff3_path = "/home/nol/BIO/workflows/testdata/crops_genomes/phyto_at/Athaliana_167_TAIR10.gene.gff3"
    # outpre = "/home/nol/BIO/workflows/testdata/crops_genomes/phyto_at/at.test"
    # generate_all_files(fasta_path, gff3_path, outpre, 500)
    cli_main()
