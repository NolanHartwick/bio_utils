#!/usr/bin/env python3

import os
import argparse
import sys
import json
from collections import defaultdict
from string import Template
import vcf as pyvcf

html_template = """<!DOCTYPE html>
<html>
    <head>
        <title>$outbase vcfstats</title>
        <script src="https://cdn.plot.ly/plotly-2.0.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous"></script>
        <script src="https://unpkg.com/@upsetjs/bundle"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
            integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
        <style>
            .collapsible {
                background-color: #eee;
                color: #444;
                cursor: pointer;
                padding: 8px;
                width: 100%;
                border: none;
                text-align: left;
                outline: none;
                font-size: 15px;
            }

            .active, .collapsible:hover {
                background-color: #ccc;
            }

            .content {
                padding: 0 18px;
                display: none;
                overflow: hidden;
                background-color: #f1f1f1;
            }

            .center {
                display: flex;
                align-items: center;
                justify-content: center;
            }
        </style>
    </head>
    <body>
        <h1 style="text-align:center">$outbase vcfstats</h1>
        <div class="container">
            <div class="row" id="allsamps"></div>
            <div class="row" id="allsamps_cap">
                <button type="button" class="collapsible center">Caption</button>
                <div class="content">
                    <p>A count of the number of non-ref alleles that appear for each sample.
                    If a loci is diploid and homozygous alt, it would contribute 2 points.
                    If if its heterozygous, it would contribute 1 point</p>
                </div>
            </div>
            <div class="row" id="selector">
                <h3 style="text-align:center">More Stats (sample specific or all)</h3>
                <form id="SampleSelector" style="text-align:center">
                    <select id="selectID" onchange="draw_figures(this.value);"></select>
                </form>
            </div>
            <div class="row" id="bars">
                <div class="col-md-6" id="summary" style="min-height:30vw;"></div>
                <div class="col-md-6" id="stats" style="min-height:30vw;"></div>
            </div>
            <div class="row" id="bars_cap_row">
                <div class="col-md-6" id="summary_cap">
                    <button type="button" class="collapsible center">Caption</button>
                    <div class="content">
                        <p>A set of basic allele level stats. For each mutation type,
                        each allele is counted seperately meaning a diploid homozygous alt site
                        will count for 2 while a diploid heterozygous site will count for 1.</p>
                    </div>
                </div>
                <div class="col-md-6" id="stats_cap">
                    <button type="button" class="collapsible center">Caption</button>
                    <div class="content">
                        <p>Stats showing how common each genotype is accross loci described by your vcf.
                        Note that only loci defined in your VCF will be counted, not every location in
                        the genome. Each bar describes a different type of genotype at some loci. "AAAB"
                        for example implies that the loci is tetraploid and one of its 4 chromosomes has
                        the alt allele. "other" is a catch all that mostly captures loci with more than
                        one alt allele, "AABC" for example.</p>
                    </div>
                </div>
            </div>
            <div class="row" id="coverages"></div>
            <div class="row" id="coverages_cap">
                <button type="button" class="collapsible center">Caption</button>
                <div class="content">
                    <p>A 2d histogram showing you the the coverage stats accross the loci in
                    your vcf by coverage of the non-major allele and total coverage. For diploid organisms,
                    you should see a single peak. For tetraploid organisms, you should see two peaks at
                    roughly 0.25 and 0.5 respectively. </p>
                </div>
            </div>
            <div class="row" id="hists">
                <div class="col-md-6" id="tot_hist" style="min-height:30vw;"></div>
                <div class="col-md-6" id="rel_hist" style="min-height:30vw"></div>
            </div>
            <div class="row" id="hists_cap">
                <div class="col-md-6" id="tot_hist_cap">
                    <button type="button" class="collapsible center">Caption</button>
                    <div class="content">
                        <p>A histogram representing total read depth at each heterozygous loci in your VCF.</p>
                    </div>
                </div>
                <div class="col-md-6" id="rel_hist_cap">
                    <button type="button" class="collapsible center">Caption</button>
                    <div class="content">
                        <p>A histogram representing the ratio of the read depth for the most common allele at each heterozygousloci to the total read depth at the loci</p>
                    </div>
                </div>
            </div>
        </div>
        <script>
            data=$stats

            var coll = Array.from(document.getElementsByClassName("collapsible"))
            coll.forEach(function (e){
                e.addEventListener("click", function() {
                    this.classList.toggle("active");
                    var content = this.nextElementSibling;
                    if (content.style.display === "block") {
                        content.style.display = "none";
                    } else {
                        content.style.display = "block";
                    }
                })
            })

            function genOpt(s) {
                var r = document.createElement("option")
                r.text = s
                r.value = s
                return r
            }

            const zip = (arr, ...arrs) => arr.map((val, i) => arrs.reduce((a, arr) => [...a, arr[i]], [val]));

            samples = Object.keys(data)
            selector = document.getElementById("selectID")
            samples.forEach(s => selector.appendChild(genOpt(s)))
            selector.selectedIndex = samples.indexOf("all_samples")

            function draw_summary(samp) {
                summary_data = [{
                    type: 'bar',
                    x: Object.values(data[samp]['STAT']),
                    y: Object.keys(data[samp]['STAT']).map(x => x + " "),
                    orientation: 'h'
                }]
                summary_layout = {
                    title: {text: "Summary of Variants"},
                    xaxis: {title: {text: "Count(non-ref variants)"} }
                }
                return Plotly.newPlot('summary', summary_data, summary_layout)
            }

            function draw_geno2(samp){
                var gt = Object.entries(data[samp]["GT"]).sort().reverse()
                var mapper = {"0": "A", "1": "B"}
                var y = gt.map(e => e[0]).map(function (s) {
                    if(s === "other") {
                        return "other "
                    } else {
                        return Array.from(s).map(c => mapper[c]).join("") + " "
                    }
                })
                geno_data = [{
                    type: "bar",
                    x: gt.map(e => e[1]),
                    y: y,
                    orientation: 'h'
                }];
                geno_layout = {
                    title: { text: "Summary of Loci by Genotype"},
                    xaxis: {title: {text: "Count(loci)"}}
                }
                return Plotly.newPlot('stats', geno_data, geno_layout)
            }

            function get_AD_data(samp) {
                var x = []
                var y = []
                var z = []
                Object.entries(data[samp]["HIST"]["AD"]).forEach(function (e1) {
                    maindepth = e1[0]
                    Object.entries(e1[1]).forEach(function (e2) {
                        x.push(parseInt(maindepth))
                        y.push(parseInt(e2[0]))
                        z.push(parseInt(e2[1]))
                    })
                })
                return [x, y, z]
            }

            function draw_2dhist(samp) {
                var [x, y, z] = get_AD_data(samp)
                var rel = zip(x, y).map(e => (e[1] - e[0]) / e[1])
                log10 = Math.log(10)
                logz = z.map(v => Math.log(v + 1) / log10)
                logy = y.map(e => Math.log(e + 1) / log10)
                cov_data = [{
                    hovertemplate: "SAMPLE=" + samp + "<br>Allele Ratio=%{x}<br>Total Depth=%{y}<br>log10(COUNT+1)=%{z}<extra></extra>",
                    type: "histogram2d",
                    x: rel, y: logy, z: logz,
                    histfunc: "sum",
                    nbinsx: 100,
                    nbinsy: 50
                }]
                cov_layout = {
                    title: {
                        text: "Coverage of Heterozygous Loci"
                    },
                    yaxis: {title: {text: "Log(Total Read Depth + 1)"}},
                    xaxis: {title: {text: "Ratio of Minor Allele(s) Depth to Total Depth"}}
                }
                return Plotly.newPlot('coverages', cov_data, cov_layout)
            }

            function draw_tot_hist(samp) {
                var [x, y, z] = get_AD_data(samp)
                cov_data = [{
                    type: "histogram",
                    x: y, y: z,
                    histfunc: "sum",
                    nbinsx: Math.floor(Math.max(...y)/5) + 1
                }]
                cov_layout = {
                    title: {text: "Total Coverage at Het Loci"},
                    yaxis: {title: {text: "Count(loci)"} },
                    xaxis: {title: {text: "Read Depth"} }
                }
                return Plotly.newPlot('tot_hist', cov_data, cov_layout)
            }

            function draw_main_hist(samp) {
                var [x, y, z] = get_AD_data(samp)
                cov_data = [{
                    type: "histogram",
                    x: x, y: z,
                    histfunc: "sum",
                    nbinsx: Math.floor(Math.max(...x)/5) + 1
                }]
                cov_layout = {
                    title: {text: "Primary Allele Coverage at Het Loci"},
                    yaxis: {title: {text: "Count(loci)"} },
                    xaxis: {title: {text: "Read Depth for most Common Allele"} }
                }
                return Plotly.newPlot('main_hist', cov_data, cov_layout)
            }

            function draw_rel_hist(samp) {
                var [x, y, z] = get_AD_data(samp)
                var rel = zip(x, y).map(e => e[0] / e[1])
                cov_data = [{
                    type: "histogram",
                    x: rel, y: z,
                    histfunc: "sum",
                    nbinsx: 50,
                }]
                cov_layout = {
                    title: {text: "Relative Coverage at Het Loci"},
                    xaxis: {range: [0, 1], title: "Primary / Total"},
                    yaxis: {title: {text: "Count(loci)"} }
                }
                return Plotly.newPlot('rel_hist', cov_data, cov_layout)
            }

            function draw_all_samps() {

                counts = samples.filter(s => s != "all_samples").map(
                    s => Object.values(data[s]["STAT"]).reduce( (a, b) => a + b)
                )
                summary_data = [{
                    type: 'bar',
                    x: counts,
                    y: samples.map(x => x + " "),
                    orientation: 'h'
                }]
                summary_layout = {
                    title: {text: "Count of Variants by Sample"},
                    margin: {l: Math.max(...(summary_data[0].y.map(e => e.length))) * 8},
                    xaxis: {title: {text: "Count(non-ref variants)"}}
                }
                return Plotly.newPlot('allsamps', summary_data, summary_layout)
            }

            function draw_figures(samp) {
                console.log(samp)
                draw_all_samps()
                draw_summary(samp)
                draw_geno2(samp)
                draw_2dhist(samp)
                draw_tot_hist(samp)
                // draw_main_hist(samp)
                draw_rel_hist(samp)
            }

            draw_figures("all_samples")
        </script>
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
            integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
            crossorigin="anonymous"></script>
    </body>
</html>
"""

def get_args():
    """ Parse arguments for the cli version of subset_fastq
    """
    parser = argparse.ArgumentParser(description=(
        'Computes samplewise stats for some VCF, dumps those stats to a '
        'json document and generates an html page to visualize the stats. '
    ))
    parser.add_argument(
        "vcf",
        default=sys.stdin,
        type=lambda f: open(f, "r"),
        nargs="?",
        help="path to vcf file. If not provided, will read from stdin"
    )
    parser.add_argument(
        "-o", "--outbase",
        default="vcfstats",
        help="output prefix. Defaults to 'vcfstats'"
    )
    return parser.parse_args()


def istitv(ref, alt):
    ref, alt = str(ref), str(alt)
    transitions = {("A", "G"), ("C", "T"), ("G", "A"), ("T", "C")}
    if(len(alt) == 1 and len(ref) == 1):
        if((ref, alt) in transitions):
            return "transition"
        else:
            return "transversion"
    else:
        return "NA"


def titvlabels(ref, alts):
    if(len(ref) != 1):
        return ["NA"] + ["NA"] * len(alts)
    else:
        return ["NA"] + [istitv(ref, a) for a in alts]
        

def collect_stats(vcf_reader):
    stats = {
        s: {
            "STAT": defaultdict(lambda: 0),
            # "GT": defaultdict(lambda: defaultdict(lambda: 0)),
            "GT": defaultdict(lambda: 0),
            "HIST": defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: 0)))
        }
        for s in vcf_reader.samples + ["all_samples"]
    }
    for record in vcf_reader:
        rec_type = record.INFO.get("TYPE", ["NA"])[0]
        titv_types = titvlabels(record.REF, record.ALT)
        for s in record.samples:
            if(s.gt_alleles[0] is None):
                continue
            sid = s.sample

            # variant level stats
            for a in s.gt_alleles:
                a = int(a)
                titvt = titv_types[a]
                if(titvt != "NA"):
                    stats[sid]["STAT"][titvt] += 1
                    stats["all_samples"]["STAT"][titvt] += 1
                if(a != 0):
                    stats[sid]["STAT"][rec_type] += 1
                    stats["all_samples"]["STAT"][rec_type] += 1

            # loci level stats
            alleles = "".join(s.gt_alleles)
            # stats[sid]["GT"][alleles][rec_type] += 1
            if(all(a in "01" for a in alleles)):
                stats[sid]["GT"][alleles] += 1
                stats["all_samples"]["GT"][alleles] += 1
            else:
                stats[sid]["GT"]["other"] += 1
                stats["all_samples"]["GT"]["other"] += 1

            # Het allele depths
            ishet = any(alleles[0] != a for a in alleles)
            if(hasattr(s.data, "AD") and ishet):
                depths = [int(i) for i in s.data.AD if i is not None]
                if(len(depths) != 0):
                    totdepth = sum(depths)
                    maindepth = max(depths)
                    stats[sid]["HIST"]["AD"][maindepth][totdepth] += 1
                    stats["all_samples"]["HIST"]["AD"][maindepth][totdepth] += 1
    return stats


def main(opened_vcf, outbase):
    vcf_reader = pyvcf.Reader(opened_vcf)
    stats = collect_stats(vcf_reader)
    jsonstats = json.dumps(stats, indent=4, sort_keys=True)
    with open(f"{outbase}.stats.json", "w") as f:
        f.write(jsonstats)
    htmltext = Template(html_template).substitute(
        stats=jsonstats,
        outbase=os.path.basename(outbase)
    )
    outpath = outbase + ".stats.html"
    with open(outpath, "w") as fout:
        fout.write(htmltext)


if(__name__ == "__main__"):
    args = get_args()
    vcf = args.vcf
    outbase = args.outbase
    # f = "/home/nol/BIO/workflows/testdata/ad77271a.v5.variants.head10k.vcf"
    # vcf = open(f)
    # outbase = "test"
    main(vcf, outbase)
