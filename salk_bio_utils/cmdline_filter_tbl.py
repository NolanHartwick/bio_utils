import pandas
import sys
import argparse
import operator


def parse_expression(s):
    s = s.split(":")
    comp = s[-2]
    val = s[-1]
    try:
        val = float(val)
    except ValueError:
        pass
    key = ":".join(s[:-2])
    funcs = {
        "lt": operator.lt,
        "le": operator.le,
        "eq": operator.eq,
        "ne": operator.ne,
        "ge": operator.ge,
        "gt": operator.gt,
    }
    return key, funcs[comp], val


def lambda_expression(exp):
    return lambda s: eval(exp)


def filter_table_args():
    parser = argparse.ArgumentParser(
        description="filters a tsv table based on a set of expressions"
    )
    parser.add_argument(
        "table",
        type=lambda s: open(s) if(s != "-") else sys.stdin,
        help="cycling results table. Use - to read from stdin"
    )
    parser.add_argument(
        "expression",
        nargs="+",
        type=lambda_expression,
        help=(
            "a pythonic expression used to filter a table. rows from the "
            "table will be accessible as <s> and the expression should "
            "evaluate to a boolean. For example, to filter based on a "
            "column named haystack_fdr you might use 's.haystack_fdr<0.05'"
        )
    )
    parser.add_argument(
        "--sep", "-s",
        default="\t",
        help="column seperator. defaults to tab"
    )
    parser.add_argument(
        "-c", "--chunksize",
        default=10000,
        type=int,
        help="how many rows of the table to read into memory at a time"
    )
    return parser.parse_args()


def cmdline_filter_table():
    args = filter_table_args()
    dfs = pandas.read_csv(args.table, sep=args.sep, chunksize=args.chunksize)
    for df in dfs:
        for l in args.expression:
            df = df[df.apply(l, axis=1)]
        df.to_csv(sys.stdout, sep=args.sep, index=None)


