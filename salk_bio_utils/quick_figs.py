import argparse
import doctest
import pandas
from plotly import express


def purge_haplotigs_hist_args():
    parser = argparse.ArgumentParser(
        description="Draws a histogram from a gencov file."
    )
    parser.add_argument(
        "gencov",
        help="a gencov file"
    )
    parser.add_argument(
        "-o", "--output",
        help="output file location. Defaults to <gencov>.html",
        default=None
    )
    args = parser.parse_args()
    if(args.output is None):
        args.output = args.gencov + ".html"
    return args


def purge_haplotigs_hist():
    args = purge_haplotigs_hist_args()
    df = pandas.read_csv(args.gencov, sep="\t", header=None, names=['CID', 'COV', 'BASES'])
    stats = df.groupby('COV').BASES.sum().to_frame().reset_index()
    stats.columns = ['COVERAGE', 'BASES']
    fig = express.bar(stats, x="COVERAGE", y="BASES", title=args.gencov)
    fig.write_html(args.output)

