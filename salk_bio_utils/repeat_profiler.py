import argparse
import sys
import pandas
import re
from Bio import SeqIO


def parse_fasta(fhandle):
    df = pandas.DataFrame([
        {'ID': rec.id, 'SEQ': str(rec.seq)}
        for rec in SeqIO.parse(fhandle, "fasta")
    ])
    return df


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "searches for a specific kmer and returns bedgraph showing occurences of kmer in specified window"
        ),
    )
    parser.add_argument(
        "fasta",
        type = lambda f: sys.stdin if(f == '-') else open(f),
        help=(
            "a fasta file to search. Use - to read stdin"
        )
    )
    parser.add_argument(
        'kmer',
        type = lambda s: s.upper(),
        help="a kmer to search for"
    )
    parser.add_argument(
        '-w', '--window_size',
        type=int,
        default=10000,
        help="the default window size"
    )
    return parser.parse_args()


def reverse_comp(kmer, language=dict(zip('ACGT', 'TGCA'))):
    return ''.join(language[k] for k in kmer)[::-1]


def search_kmers(fastadf, kmer):
    rekmer = re.compile(kmer + '|' + reverse_comp(kmer))
    return pandas.DataFrame({
        'ID': fastadf.ID,
        'matches': fastadf.SEQ.apply(lambda s: [m.start() for m in rekmer.finditer(s)])
    })


def kmer_profile(seq, kmer, window):
    ''' returns frequency of kmer in seq using non-overlapping windows of size window
    '''
    substr = [seq[i:i+window] for i in range(0, len(seq), window)]
    return [
        (s.count(kmer) + s.count(reverse_comp(kmer))) / len(s)
        for s in substr
    ]


def main():
    args = get_args()
    df = parse_fasta(args.fasta)
    kmer = args.kmer
    ws = args.window_size
    '''
    result = search_kmers(df, kmer)
    for ctg, hits in zip(result.ID, result.matches):
        for h in hits:
            sys.stdout.write(f'{ctg}\t{h}\t{h+1}\n')
    '''
    df['profiles'] = df.SEQ.apply(lambda s: kmer_profile(s, kmer, ws))
    for ctg, seq, profile in zip(df.ID, df.SEQ, df.profiles):
        for i, r in enumerate(profile):
            start = i * ws
            end = min((i + 1) * ws, len(seq))
            sys.stdout.write(f'{ctg}\t{start}\t{end}\t{r}\n')


if(__name__ == "__main__"):
    main()

