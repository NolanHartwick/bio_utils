#!/usr/bin/env python3

import argparse
import sys
import doctest
import pandas
from scipy.stats import fisher_exact
from statsmodels.stats.multitest import multipletests


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "Does overrepresentation analysis between two sets of labels for some items"
        ),
    )
    parser.add_argument(
        "labels1",
        help=(
            "a tsv file. First column should be gene ids, second column "
            "should be labels associated with genes"
        )
    )
    parser.add_argument(
        "labels2",
        help=(
            "a tsv file. First column should be gene ids, second column "
            "should be labels associated with genes"
        )
    )
    parser.add_argument(
        "-i", "--items",
        default=None,
        help=(
            "optional list of genes that aren't really in either of your sets"
        )
    )
    args = parser.parse_args()
    return args


def fisher_test(stats):
    """ do a fisher test given stats where stats is a tuple containing ints
        representing...

        (intersecting_genes, left_genes, right_genes, all_genes)

        >>> s = (5, 7, 6, 100)
        >>> odds, p = fisher_test(s)
        >>> print(f"{p:0.8f}")
        0.00000164
        >>> print(f"{odds:0.1f}")
        230.0
    """
    intersection, left, right, allcount = stats
    t = [
        [intersection, left - intersection],
        [right - intersection, allcount - left - right + intersection]
    ]
    return fisher_exact(t, alternative="greater")


def test_relationships(rels1, rels2, norels):
    """ Given two sets of relationships with the same domain,
        compute dependance between them.
    """
    r1c0, r1c1 = rels1.columns[0], rels1.columns[1]
    r2c0, r2c1 = rels2.columns[0], rels2.columns[1]
    s1 = rels1.groupby(r1c1)[r1c0].apply(set)
    s2 = rels2.groupby(r2c1)[r2c0].apply(set)
    allgenes = len(set(rels1[r1c0]) | set(rels2[r2c0]) | norels) 
    pair_counts = pandas.DataFrame(
        [
            [c1, c2, (len(sa & sb), len(sa), len(sb), allgenes)]
            for c1, sa in s1.items()
            for c2, sb in s2.items()
        ],
        columns=['C1','C2', "STATS"]
    )
    pair_counts['results'] = pair_counts.STATS.apply(fisher_test)
    results = pandas.DataFrame({
        "CLASS1": pair_counts.C1,
        "CLASS2": pair_counts.C2,
        "INTERSECTION": pair_counts.STATS.str[0],
        "C1_SIZE": pair_counts.STATS.str[1],
        "C2_SIZE":  pair_counts.STATS.str[2],
        "TOT_SIZE": pair_counts.STATS.str[3],
        "ODDS_RATIO": pair_counts.results.str[0],
        "FISHER_P": pair_counts.results.str[1],
    })
    results["FISHER_FDR"] = multipletests(results.FISHER_P, method="fdr_bh")[1]
    return results


def main():
    args = get_args()
    l1 = pandas.read_csv(args.labels1, sep="\t", header=None)
    l2 = pandas.read_csv(args.labels2, sep="\t", header=None)
    if(args.items is not None):
        gs = set(pandas.read_csv(args.items, header=None)[0])
    else:
        gs = set()
    l1_genes = set(l1[0])
    l2_genes = set(l2[0])
    sys.stderr.write(f"Testing for enlarged overlaps between:\n")
    sys.stderr.write(f"{args.labels1}\n")
    sys.stderr.write(f"{args.labels2}\n")
    sys.stderr.write(f"\nSet stats:\n")
    sys.stderr.write(f"{args.labels1} : {len(l1_genes)}\n")
    sys.stderr.write(f"{args.labels2} : {len(l2_genes)}\n")
    sys.stderr.write(f"{args.labels1} - {args.labels2} : {len(l1_genes - l2_genes)}\n")
    sys.stderr.write(f"{args.labels2} - {args.labels1} : {len(l2_genes - l1_genes)}\n")
    sys.stderr.write(f"{args.labels1} & {args.labels2} : {len(l2_genes & l1_genes)}\n")
    sys.stderr.write(f"Total Items : {len(l2_genes | l1_genes | gs)}\n")
    results = test_relationships(l1, l2, gs)
    results.to_csv(sys.stdout, sep="\t", index=None)


if(__name__ == "__main__"):
    doctest.testmod()
