import pandas
import sys
import argparse


def extract_readqc_stats_args():
    parser = argparse.ArgumentParser(
        description="extracts summary stats from a bunch of fastqc/nanoplot html results"
    )
    parser.add_argument(
        "html",
        nargs="+",
        help="some number of nanoplot/fastqc html files"
    )
    parser.add_argument(
        "-n", "--names",
        nargs="*",
        help="Names for each html file. If not present, the filenames themselves are used",
        default=None
    )
    args = parser.parse_args()
    if(args.names is None):
        args.names = args.html
    return args


def extract(htmlpath, name):
    if(htmlpath.endswith('fastqc.html')):
        df = pandas.read_html(htmlpath)[0]
        return {
            "READSET": name,
            "meanLength": int(float(df.iloc[5, 1])),
            "meanQ": None,
            "medianLength": None,
            "medianQ": None,
            "nReads": int(float(df.iloc[3, 1])),
            "RN50": None,
            "stdLength": None,
            "totalBases": int(float(df.iloc[3, 1])) * int(float(df.iloc[5, 1])),
        }
    elif(htmlpath.endswith('nanoplot.report.html')):
        df = pandas.read_html(htmlpath)[0]
        return {
            "READSET": name,
            "meanLength": float(df.iloc[1, 1]),
            "meanQ": float(df.iloc[2, 1]),
            "medianLength": float(df.iloc[3, 1]),
            "medianQ": float(df.iloc[4, 1]),
            "nReads": int(float(df.iloc[5, 1])),
            "RN50": int(float(df.iloc[6, 1])),
            "stdLength": float(df.iloc[7, 1]),
            "totalBases": int(float(df.iloc[8, 1])),
        }
    else:
        raise ValueError(f"extension not recongized for file: {htmlpath}")


def extract_readqc_stats_main():
    args = extract_readqc_stats_args()
    ret = pandas.DataFrame([extract(f, n) for f, n in zip(args.html, args.names)])
    ret.to_csv(sys.stdout, sep="\t", index=None)

