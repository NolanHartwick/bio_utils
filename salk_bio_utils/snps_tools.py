import argparse
import sys
import pandas
from collections import deque
import json
from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord



def suffix_int(s: str) -> int:
    """ take suffix into account when converting s to int. suffixes are case
        insensitive. supported suffixes are below. Deciminal portion, if 
        present, is clipped after applying the suffix multiplier

        Suffix : Multiplier
        k : 10 ** 3
        m : 10 ** 6
        g : 10 ** 9
        t : 10 ** 12
        p : 10 ** 15

        >>> suffix_int("10k")
        10000
        >>> suffix_int("5K")
        5000
        >>> suffix_int("152m")
        152000000
        >>> suffix_int("2.3G")
        2300000000
        >>> suffix_int("4")
        4
        >>> suffix_int("4.2222K")
        4222
    """
    mulipliers = {
        "k": 10**3, "K": 10**3,
        "m": 10**6, "M": 10**6,
        "g": 10**9, "G": 10**9,
        "t": 10**12, "T": 10**12,
        "p": 10**15, "P": 10**15
    }
    if(s[-1] in mulipliers):
        return int(mulipliers[s[-1]] * float(s[:-1]))
    else:
        return int(float(s))


def load_genome_data(gff3, fasta=None):
    seq_dict = None if(fasta is None) else SeqIO.to_dict(SeqIO.parse(fasta, "fasta"))
    ret = list(GFF.parse(gff3, base_dict=seq_dict))
    return ret


def get_children(f, ftype, include_self=True):
    """ Recursively move through sub_features of f and return a set of all
        sub_features matching ftype
    """
    if(include_self):
        to_process = deque([f])
    else:
        to_process = deque(f.sub_features)
    ret = set()
    processed = set()
    while(len(to_process) > 0):
        f = to_process.popleft()
        if(f.type == ftype):
            ret.add(f)
        processed.add(f)
        for s in f.sub_features:
            if(s not in processed):
                to_process.append(s)
    return ret


def get_genes(record, keys):
    """ Return a set of genes associated with some seqRecord object
    """
    return set(g for f in record.features for g in get_children(f, keys["gene"]))


def snps_matching_genes_cli():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that cross references snps and a gff3 file meant to "
            "provide information on genes near significant gwas snps"
        )
    )
    parser.add_argument(
        "gff3",
        type=argparse.FileType('r'),
        help="gff3 containing gene models"
    )
    parser.add_argument(
        "snps",
        type=argparse.FileType('r'),
        help="a tsv file containing significant snps. Columns: SNP, Chromosome, Position"
    )
    parser.add_argument(
        "annots",
        type=argparse.FileType('r'),
        help="a tsv file containing annotations for genes in the gff3"
    )
    parser.add_argument(
        "-d", "--distance",
        type=suffix_int,
        help="a number of bases up/down from snps to search for genes/snps",
        default=100000
    )
    parser.add_argument(
        "-f", '--filter_genes',
        type=lambda s : set(open(s).read().split()),
        default=None,
        help="an optional file containing a list of gene ids. if present, only genes from this list will be considered"
    )
    parser.add_argument(
        "-c", '--column_gid',
        default="locusName",
        help="column to use when looking up gene ids from annots"
    )
    parser.add_argument(
        "-g", '--gene_feature',
        default="gene",
        help="default feature type to extract from gff3"
    )
    args = parser.parse_args()
    ret = snps_matching_genes_main(
        args.gff3,
        args.snps,
        args.annots,
        args.distance,
        args.filter_genes,
        args.column_gid,
        args.gene_feature
    )
    ret.to_csv(sys.stdout, sep="\t")


def intersect(start, stop, locations):
    return (locations > start)&(locations < stop)


def lowest_contained(g, pos):
    """ Given a gff3 feature, identify the 'lowest' feature/subfeature
        that contains pos. Exploits BFS, tie goes to 'rightmost' node.
    """
    to_process = deque([g])
    best = None
    while(len(to_process) > 0):
        cur = to_process.popleft()
        if(pos > cur.location.nofuzzy_start and pos < cur.location.nofuzzy_end):
            best = cur
            for f in cur.sub_features:
                to_process.append(f)
    if(best is not None):
        return best.type
    else:
        return None


def interval_distance(start, stop, pos):
    if(pos < start):
        return pos - start
    elif(pos > stop):
        return pos - stop
    else:
        return 0

def snps_matching_genes_main(gff, snps, annots, dist, filter, cid, gf):
    gff = load_genome_data(gff)
    genes = {
        record.id: {g.id: g for f in record.features for g in get_children(f, gf)}
        for record in gff
    }
    if(filter is not None):
        genes = {
            rid: {gid: g for gid, g in gd.items() if g in filter}
            for rid, gd in genes.items()
        }
    snps = pandas.read_csv(snps, sep="\t")
    if(len(set(snps.Chromosome) & set(genes)) == 0):
        sys.stderr.write("WARNING: No overlap between snp contigs and gene contigs\n")
    ret = {}
    for rid, gs in genes.items():
        rsnps = snps[snps.Chromosome == rid]
        for g in gs.values():
            start, stop = g.location.nofuzzy_start, g.location.nofuzzy_end
            overlaps = rsnps[intersect(start - dist, stop + dist, rsnps.Position)]
            if(len(overlaps) > 0):
                feats = [lowest_contained(g, p) for p in overlaps.Position]
                distance = [interval_distance(start, stop, p) for p in overlaps.Position]
                ret[g.id] = {
                    "CTGID": rid,
                    "START": start,
                    "STOP": stop,
                    "NSNPS": len(overlaps.SNP),
                    "SNPS": list(overlaps.SNP),
                    "POSITIONS": list(overlaps.Position),
                    "FEATURES": feats,
                    "DISTANCES": distance
                }
    ret = pandas.DataFrame(ret).transpose()
    annots = pandas.read_csv(annots, sep="\t").set_index(cid)
    if(len(set(annots.index) & set(gid for _, gs in genes.items() for gid in gs)) == 0):
        sys.stderr.write("WARNING: No overlap between gff3 gene ids and annotation gene ids\n")
    ret = ret.merge(annots, how="left", left_index=True, right_index=True)
    return ret

