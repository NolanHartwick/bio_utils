from setuptools import setup
import os


setup(
    name="salk_bio_utils",
    version="0.23.3",
    packages=["salk_bio_utils"],
    install_requires=[
        "bcbio-gff",
        "biopython",
        "pandas",
        "scipy",
        "statsmodels",
        "plotly",
        "lxml",
        "sourmash",
        "PyVCF3",
        "boto3"
    ],
    entry_points={
        "console_scripts": [
            "test_overrep = salk_bio_utils.test_overrep:main",
            "filter_tbl = salk_bio_utils.cmdline_filter_tbl:cmdline_filter_table",
            "gencov_plot = salk_bio_utils.quick_figs:purge_haplotigs_hist",
            "cross_reference_snps = salk_bio_utils.snps_tools:snps_matching_genes_cli",
            "summarize_readqc = salk_bio_utils.readqc_summary:extract_readqc_stats_main",
            "repeat_profiler = salk_bio_utils.repeat_profiler:main"
        ]
    },
    scripts= [os.path.join("scripts", f) for f in os.listdir("scripts")]
)
