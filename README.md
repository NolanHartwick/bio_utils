# salk_bio_utils
A collection of random scripts I've collected/written that I've found useful for various bioinformatics stuff

## installation
```
pip install git+https://gitlab.com/NolanHartwick/bio_utils.git

```
## Better Installation
```
conda create -n bio_utils -c conda-forge \
    pip bcbio-gff biopython pandas scipy statsmodels \
    plotly lxml sourmash pyvcf3 boto3 meryl=1.4.1
conda activate bio_utils
pip install git+https://gitlab.com/NolanHartwick/bio_utils.git 
```


## Scripts

#### FastaToTbl
```
# converts a fasta format file to a two column tsv
FastaToTbl <some>.fasta > <some>.tbl
```

#### TblToFasta
```
# converts a two column tsv to a fasta format file
TblToFasta <some>.tbl > <some>.fasta
```

#### unsoftmask
```
# replaces all lowercase acgt with upercase ACGT
unsoftmask <some>.softmasked.fasta > <some>.fasta
```

#### dgenies_index.py
```
$ dgenies_index.py -h
usage: dgenies_index.py [-h] -i INPUT -n NAME -o OUTPUT

Split huge contigs

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Input fasta file
  -n NAME, --name NAME  Input name
  -o OUTPUT, --output OUTPUT
                        Output index file
```

#### extract_seq.py

```
$ extract_seq.py -h
usage: extract_seq.py [-h] [-o OUTPREFIX] [--promoter_length PROMOTER_LENGTH] [--gene_feature GENE_FEATURE] [--cds_feature CDS_FEATURE] [--mrna_feature MRNA_FEATURE]
                      [--source {phytozome,other}]
                      fasta gff3

a tool that extracts biological sequences from a fasta file based on a provided gff3.

positional arguments:
  fasta                 fasta file matching the gff3
  gff3                  gff3 containing features to use

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPREFIX, --outprefix OUTPREFIX
                        a prefix to use for generating output files. Defaults to gff3 path after suffix trimming
  --promoter_length PROMOTER_LENGTH
                        default length for promoter region to extract
  --gene_feature GENE_FEATURE
                        NI
  --cds_feature CDS_FEATURE
                        NI
  --mrna_feature MRNA_FEATURE
                        NI
  --source {phytozome,other}, -s {phytozome,other}
                        What format is the gff3 in
```

#### fix_fasta.py
```
$ fix_fasta.py -h
usage: fix_fasta.py [-h] [-s SEP] [-t THRESH] [-c CHR] [-o OTHER] [-j SPECIALS_JSON] [-n] [-m MAP] [-g GAP] fasta

a tool that quickly reads a fasta file, sorts, and renames the contigs.

positional arguments:
  fasta                 fasta file you wish to make modifications to

optional arguments:
  -h, --help            show this help message and exit
  -s SEP, --sep SEP     seperator used to split contig ids from contig info
  -t THRESH, --thresh THRESH
                        An value used to seperate contigs from chromosomes. If the value is greater than or equal to 1, interpret it as an int. Contigs with length
                        greater than value will be labelled chromosomes. If the value is less than 1, interpret it as a fraction of total sequence length. For example, if
                        the value is 0.01, then any contig with length greater than 1 percent of the total sequence length will be called a chromosome
  -c CHR, --chr CHR     Prefix used to name chromosomes. Contigs with length greater than thresh will have name <chr><k> where k is an incrimenting integer id.
  -o OTHER, --other OTHER
                        Prefix used to name contigs. Contigs with length less than thresh will have name <other><k> where k is an incrimenting integer id.
  -j SPECIALS_JSON, --specials_json SPECIALS_JSON
                        A path to a json file encoding a mapping from previous contig names to new contig names. Any contig with a name in this dictionary will use the
                        associated value as a new name. Thesecontigs wil be placed immediately following the chromosomes
  -n, --nosort          If present, current order will be preserved and numbering will based on current order with prefixes based on threshold and contig length. Default
                        behavior is to sort by length
  -m MAP, --map MAP     A json file encoding name mappings (new -> old) will be created at the specified path. Defaults to <fasta>.names.json
  -g GAP, --gap GAP     Replace long consecutive Ns with this many N. Basically collapses big gaps to a single standard length. Use a value <=0 to disable and preserve
                        gaps. Default=500

USAGE: python fix_fasta some.fasta --sep ' ' --thresh 0.01 --chr chr --other contig > new.fasta
```

#### get_chrom_sizes.py
```
$ get_chrom_sizes.py -h
usage: get_chrom_sizes.py [-h] fasta

a tool that reports the length of each entry in a fasta file

positional arguments:
  fasta       A fasta file for which you want stats, use - for stdin

optional arguments:
  -h, --help  show this help message and exit
```

#### juicer_med2long.py
```
$ juicer_med2long.py -h
usage: juicer_med2long.py [-h] contacts

Converts a juicer 'medium' style contacts table into a long' style table with some dummy values

positional arguments:
  contacts    A table with medium style juicer contacts

optional arguments:
  -h, --help  show this help message and exit
```

#### merge_busco_tables.py
```
$ merge_busco_tables.py -h
usage: merge_busco_tables.py [-h] [--db DB] [--summary SUMMARY] busco_dir [busco_dir ...]

Combines information from the full_table output of multiple busco runs into a single table and writes to stdout. Optionally writes a merged summary to the specified path.

positional arguments:
  busco_dir             paths to busco output dir

optional arguments:
  -h, --help            show this help message and exit
  --db DB, -d DB        Path to ODB10 busco databse used in the busco runs. If provided, informaiton and links for busco genes will be embedded in output table
  --summary SUMMARY, -s SUMMARY
                        output path where a summary table will be created if desired
```

#### merge_samtools_stats.py
```
$ merge_samtools_stats.py -h
usage: merge_samtools_stats.py [-h] bamstats [bamstats ...]

a tool that merges SN stats from multiple 'samtools stats' into a single table

positional arguments:
  bamstats    a set of samtools stats outputs

optional arguments:
  -h, --help  show this help message and exit
```

#### reverse_paf.py
```
$ reverse_paf.py -h
usage: reverse_paf.py [-h] paf

This program will invert a paf file, swapping query and target

positional arguments:
  paf         path to paf file

optional arguments:
  -h, --help  show this help message and exit
```

#### subset_fastq.py
```
$ subset_fastq.py -h
usage: subset_fastq.py [-h] [--minreads MINREADS] [--minlen MINLEN] [--json_subset JSON_SUBSET] [--seed SEED] [--with_replace] [--keep_longest] fastq

This program will subset reads from a fastq (.gz) file.

positional arguments:
  fastq                 the path to a fastq file that needs to be processed

optional arguments:
  -h, --help            show this help message and exit
  --minreads MINREADS, -r MINREADS
                        A minimum number of reads to subset down to. Defaults to 0.
  --minlen MINLEN, -l MINLEN
                        a minimum length of output reads to subset down to. Defaults to 0.
  --json_subset JSON_SUBSET, -j JSON_SUBSET
                        if --minreads or --minlen are provided, the computed subset will be written to the path JSON_SUBSET. Else, read the json file at JSON_SUBSET and
                        use that subset definiton to subset the provided fastq
  --seed SEED, -s SEED  If provided, sets the RNG seed
  --with_replace, -w    Whether or not to sample with replacement
  --keep_longest, -k    Subset down to a longest set of reads instead of subsetting randomly. If present, this flag overrules --with_replace.
```

#### vcfstats.py

```
$ python salk_bio_utils/vcfstats.py -h
usage: vcfstats.py [-h] [-o OUTBASE] [vcf]

Computes samplewise stats for some VCF, dumps those stats to a json document and generates an html page to visualize the stats.

positional arguments:
  vcf                   path to vcf file. If not provided, will read from stdin

optional arguments:
  -h, --help            show this help message and exit
  -o OUTBASE, --outbase OUTBASE
                        output prefix. Defaults to 'vcfstats'
```

#### eggnog_split_fasta.py

```
$ python salk_bio_utils/eggnog_split_fasta.py -h
usage: eggnog_split_fasta.py [-h] [-s STATS] [-i]
                             fasta eggnog filter [filter ...]

split a fasta file based on eggnog annotations of proteins by applying a
series of filters. 'i' (include) operations add sequences to the output. 'x'
(exclude) operations remove sequences from the output. If the first filter is
an exclude, all sequences will be added prior to applying the filter. If the
first filter is an include, no sequences will be added prior to applying the
filter. This tool is only compatible with salk-tm genomes/annotations.

positional arguments:
  fasta                 path to fasta file whos proteins were annotated by
                        eggnog or '-' to read from stdin
  eggnog                path to eggnog annotations
  filter                a series of filter expressions. Format should match
                        x|i:<taxid>:0.0-1.0. Should look like...
                        i:Viridiplantae:0.25 x:Bacteria:0.5

optional arguments:
  -h, --help            show this help message and exit
  -s STATS, --stats STATS
                        If present, taxid counts for each contig will be
                        written to this location
  -i, --include_nulls   If present, contigs with no annotated gene models will
                        be included in the output
```

#### test_overrep
```
$ test_overrep -h
usage: test_overrep [-h] [-i ITEMS] labels1 labels2

Does overrepresentation analysis between two sets of labels for some items

positional arguments:
  labels1               a tsv file. First column should be gene ids, second column should be labels associated with genes
  labels2               a tsv file. First column should be gene ids, second column should be labels associated with genes

optional arguments:
  -h, --help            show this help message and exit
  -i ITEMS, --items ITEMS
                        optional list of genes that aren't really in either of your sets
```

#### filter_tbl
```
$ filter_tbl -h
usage: filter_tbl [-h] [--sep SEP] [-c CHUNKSIZE] table expression [expression ...]

filters a tsv table based on a set of expressions

positional arguments:
  table                 cycling results table. Use - to read from stdin
  expression            a pythonic expression used to filter a table. rows from the table will be accessible as <s> and the expression should evaluate to a boolean. For example, to filter based on a column named haystack_fdr you might use 's.haystack_fdr<0.05'

optional arguments:
  -h, --help            show this help message and exit
  --sep SEP, -s SEP     column seperator. defaults to tab
  -c CHUNKSIZE, --chunksize CHUNKSIZE
                        how many rows of the table to read into memory at a time
```

#### gencov_plot
```
$ gencov_plot -h
usage: gencov_plot [-h] [-o OUTPUT] gencov

Draws a histogram from a gencov file.

positional arguments:
  gencov                a gencov file

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        output file location. Defaults to <gencov>.html
```


#### cross_reference_snps
```
$ cross_reference_snps -h
usage: cross_reference_snps [-h] [-d DISTANCE] [-f FILTER_GENES] [-c COLUMN_GID] [-g GENE_FEATURE] gff3 snps annots

a tool that extracts biological sequences from a fasta file based on a provided gff3.

positional arguments:
  gff3                  gff3 containing gene models
  snps                  a tsv file containing significant snps. Columns: SNP, Chromosome, Position
  annots                a tsv file containing annotations for genes in the gff3

optional arguments:
  -h, --help            show this help message and exit
  -d DISTANCE, --distance DISTANCE
                        a number of bases up/down from snps to search for genes/snps
  -f FILTER_GENES, --filter_genes FILTER_GENES
                        an optional file containing a list of gene ids. if present, only genes from this list will be considered
  -c COLUMN_GID, --column_gid COLUMN_GID
                        column to use when looking up gene ids from annots
  -g GENE_FEATURE, --gene_feature GENE_FEATURE
                        default feature type to extract from gff3
```

#### summarize_readqc
```
$ summarize_readqc -h
usage: summarize_readqc [-h] [-n [NAMES [NAMES ...]]] html [html ...]

extracts summary stats from a bunch of fastqc/nanoplot html results

positional arguments:
  html                  some number of nanoplot/fastqc html files

optional arguments:
  -h, --help            show this help message and exit
  -n [NAMES [NAMES ...]], --names [NAMES [NAMES ...]]
                        Names for each html file. If not present, the filenames themselves are used
```

#### merge_gff3.py
```
$ merge_gff3.py -h
usage: merge_gff3.py [-h] [-f FASTA] [-r REF] [-o OUTPREFIX] [-p PATTERN] [--gene_feature GENE_FEATURE] [--cds_feature CDS_FEATURE] [--exon_feature EXON_FEATURE]
                     [--mrna_feature MRNA_FEATURE] [--utr3_feature UTR3_FEATURE] [--utr5_feature UTR5_FEATURE]
                     alts [alts ...]

a tool that merges gene models from alt gff3s into a ref gff3.

positional arguments:
  alts                  gff3 containing gene models to merge in

optional arguments:
  -h, --help            show this help message and exit
  -f FASTA, --fasta FASTA
                        fasta file matching the gff3s
  -r REF, --ref REF     reference gff3
  -o OUTPREFIX, --outprefix OUTPREFIX
                        a prefix to use for generating output files. Defaults to 'merged'
  -p PATTERN, --pattern PATTERN
                        a python format string used for naming gene models when necessary. defaults to 'merged.{ctg}.{{num:05d}}'
  --gene_feature GENE_FEATURE
                        feature type to look for to identify gene features. Default is 'gene'
  --cds_feature CDS_FEATURE
                        feature type to look for to identify CDS features. Should be children of --mrna_feature. Default is 'CDS'
  --exon_feature EXON_FEATURE
                        feature type to look for to identify exon features. Should be children of --mrna_feature. Default is 'exon'
  --mrna_feature MRNA_FEATURE
                        feature type to look for to identify transcript features. Should be children of --gene_feature. Default is 'mRNA'
  --utr3_feature UTR3_FEATURE
                        feature type to look for to identify three prime UTR features. Should be children of --mrna_feature. Default is 'three_prime_UTR'
  --utr5_feature UTR5_FEATURE
                        feature type to look for to identify five prime UTR features. Should be children of --mrna_feature. Default is 'five_prime_UTR'
```


#### bin_hic.py
```
$ bin_hic.py
usage: bin_hic.py [-h] [-k KMERSIZE] hap1 hap2 fq1 fq2 outbase

given hap1 and hap2, bin hic fastq1 and fastq2

positional arguments:
  hap1                  fasta file representing haplotype1
  hap2                  fasta file representing haplotype1
  fq1                   fastq R1 file containing HiC reads
  fq2                   fastq R2 file containing HiC reads
  outbase               output prefix

optional arguments:
  -h, --help            show this help message and exit
  -k KMERSIZE, --kmersize KMERSIZE
                        kmer size to use
```

#### repeat_profiler
```
$ repeat_profiler -h
usage: repeat_profiler [-h] [-w WINDOW_SIZE] fasta kmer

searches for a specific kmer and returns bedgraph showing occurences of kmer in specified window

positional arguments:
  fasta                 a fasta file to search. Use - to read stdin
  kmer                  a kmer to search for

optional arguments:
  -h, --help            show this help message and exit
  -w WINDOW_SIZE, --window_size WINDOW_SIZE
                        the default window size
```

#### fix_fasta_set.py
```
$ fix_fasta_set.py -h
usage: fix_fasta_set.py [-h] [-l LENGTH_CUTOFF] [-o OUTDIR] [--chr [CHR ...]] [--ctg [CTG ...]] reference others [others ...]

Renames and reorients chromosomes in a set of fasta files

positional arguments:
  reference             an input fasta to be renamed. Will be used as reference for pairwise comparisons.
  others                other fasta files to be renamed

options:
  -h, --help            show this help message and exit
  -l LENGTH_CUTOFF, --length_cutoff LENGTH_CUTOFF
                        length cutoff to seperate chromosomes from contigs (default: 5000000)
  -o OUTDIR, --outdir OUTDIR
                        a directory path to write fixed fastas too (default: cleaned_fastas)
  --chr [CHR ...]       naming pattern for chromosomes. Keys: refid, id, num (default: ['chr{}'])
  --ctg [CTG ...]       naming pattern(s) for non chromosomes (default: ['ctg{}'])
```

#### sour_dotplot.py
```
$ sour_dotplot.py -h
usage: sour_dotplot.py [-h] [-c CHUNK_SIZE] [-o OUTPUT] [-b BORDER] [-s SCALED] reference other

rapidly creates a dotplot like visual for two genomes

positional arguments:
  reference
  other

options:
  -h, --help            show this help message and exit
  -c CHUNK_SIZE, --chunk_size CHUNK_SIZE
                        chunksize to use (default: 1000000)
  -o OUTPUT, --output OUTPUT
                        An output location to write html to. Use - for stdout (default: sour_dots.html)
  -b BORDER, --border BORDER
  -s SCALED, --scaled SCALED
```

#### gff3_to_bed_subset.py
```
$ gff3_to_bed_subset.py -h
usage: gff3_to_bed_subset.py [-h] [--outpath OUTPATH] gff3 feature_type [feature_type ...]

convert gff3 to bedfile

positional arguments:
  gff3               a gff3 file to process
  feature_type       Feature types to convert

options:
  -h, --help         show this help message and exit
  --outpath OUTPATH  path to write output too. default=stdout
```
#### get_gene_regions.py
```
$ get_gene_regions.py -h
usage: get_gene_regions.py [-h] [-o OUTPUT] [-f FEATURE] [-u UPSTREAM] [-d DOWNSTREAM] fasta gff3

a tool that extracts gene regions from a fasta file based on a provided gff3.

positional arguments:
  fasta                 fasta file matching the gff3
  gff3                  gff3 containing features to use

options:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        output path. Use '-' for stdout. (default: <_io.TextIOWrapper name='<stdout>' mode='w' encoding='utf-8'>)
  -f FEATURE, --feature FEATURE
                        feature to look for to extract region around. (default: gene)
  -u UPSTREAM, --upstream UPSTREAM
                        length upstream of feature to include in region (default: 500)
  -d DOWNSTREAM, --downstream DOWNSTREAM
                        length downstream of feature to include in region (default: 500)
```
#### get_cds.py
```
$ get_cds.py -h
usage: get_cds.py [-h] [-o OUTPUT] [-g GENE_FEATURE] [-t TRANSCRIPT_FEATURE] [-c CDS_FEATURE] [-u UPSTREAM] [-d DOWNSTREAM] fasta gff3

a tool that extracts transcripts from a fasta file based on a provided gff3.

positional arguments:
  fasta                 fasta file matching the gff3
  gff3                  gff3 containing features to use

options:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        output path. Use '-' for stdout. (default: <_io.TextIOWrapper name='<stdout>' mode='w' encoding='utf-8'>)
  -g GENE_FEATURE, --gene_feature GENE_FEATURE
  -t TRANSCRIPT_FEATURE, --transcript_feature TRANSCRIPT_FEATURE
  -c CDS_FEATURE, --cds_feature CDS_FEATURE
  -u UPSTREAM, --upstream UPSTREAM
                        length upstream of feature to include in region (default: 500)
  -d DOWNSTREAM, --downstream DOWNSTREAM
                        length downstream of feature to include in region (default: 500)
```


#### filter_paf.py
```
$ filter_paf.py -h
usage: filter_paf.py [-h] [-m MINLEN] paf

filter a paf file by alignment length

positional arguments:
  paf                   path to paf file

options:
  -h, --help            show this help message and exit
  -m MINLEN, --minlen MINLEN
                        block length filter threshold
```

#### split_gff_to_beds.py
```
$ split_gff_to_beds.py  -h
usage: split_gff_to_beds.py [-h] [-o OUTBASE] gff3

read a gff3 and split it into bed files based on the feature types

positional arguments:
  gff3                  path to gff3 file, use - for stdin

optional arguments:
  -h, --help            show this help message and exit
  -o OUTBASE, --outbase OUTBASE
                        prefix for output files
```

#### mark_gaps.py
```
$ mark_gaps.py -h
usage: mark_gaps.py [-h] fasta n

Finds gaps in FASTA file. Writes output to stdout

positional arguments:
  fasta       Path to the FASTA file
  n           Number of Ns in a row to consider as a gap

options:
  -h, --help  show this help message and exit
```

#### make_genespace_bed.py
```
$ make_genespace_bed.py -h
usage: make_genespace_bed.py [-h] [--features FEATURES [FEATURES ...]] fasta gff3

generates appropriate genespace bed given a protein fasta and a matching gff3

positional arguments:
  fasta                 fasta file matching the gff3 containing proteins
  gff3                  gff3 containing features to use

options:
  -h, --help            show this help message and exit
  --features FEATURES [FEATURES ...]
                        feature types to check against protein ids
```

#### gffstats.py
```
$ gffstats.py -h
usage: gffstats.py [-h] [--gene_feature GENE_FEATURE] [--cds_feature CDS_FEATURE] [--mrna_feature MRNA_FEATURE] gff

write json to stdout with stats describing gene models in gff file

positional arguments:
  gff                   some gff

options:
  -h, --help            show this help message and exit
  --gene_feature GENE_FEATURE
                        feature type to look for to identify gene features. Default is 'gene'
  --cds_feature CDS_FEATURE
                        feature type to look for to identify CDS features. Should be children of --mran_feature. Default is 'CDS'
  --mrna_feature MRNA_FEATURE
                        feature type to look for to identify transcript features. Should be children of --gene_feature. Default is 'mRNA'
```

#### cquesta_get_seq.py
```
$ cquesta_get_seq.py -h
usage: cquesta_get_seq.py [-h] [-o OUTBASE] [--upstream UPSTREAM] [--downstream DOWNSTREAM] fasta gff3 mRNA [mRNA ...]

a tool that extracts biological sequences from a fasta file based on a provided gff3. Formated for Cquesta

positional arguments:
  fasta                 fasta file matching the gff3
  gff3                  gff3 containing features to use
  mRNA                  mRNA features to extract

options:
  -h, --help            show this help message and exit
  -o OUTBASE, --outbase OUTBASE
                        a prefix to use for generating output files
  --upstream UPSTREAM   default length for promoter region to extract
  --downstream DOWNSTREAM
                        default length for terminal region to extract
```

#### gff_to_genbank.py
```
$ gff_to_genbank.py -h
usage: gff_to_genbank.py [-h] -g GFF -f FASTA

Convert GFF3 and FASTA files to GenBank format.

options:
  -h, --help            show this help message and exit
  -g GFF, --gff GFF     Path to the GFF3 file.
  -f FASTA, --fasta FASTA
                        Path to the FASTA file.
```

#### make_cquesta_files.sh
```
$ make_cquesta_files.sh
Usage: /home/nolhart/miniconda3/envs/misc3/bin/make_cquesta_files.sh <Locus> <atgid> <ref> <refid> <feat> <output_directory>
Example: /home/nolhart/miniconda3/envs/misc3/bin/make_cquesta_files.sh TOM1 AT4G21790 Tarvense Tarv.1014.HPI3.4.g152100.t1 CDS batches/test

Notes: This script requires you to seperately install gffread. This can be done with conda using...

    conda install -c conda-forge -c bioconda gffread

...This script is written for generating locus data for cquesta. It automates extracting the region around the gene of interest, converting the extracted region to genbank, and extracting protein, cds, and mrna sequences.

```


#### spectra_subsets.py
```
$ spectra_subsets.py -h
usage: spectra_subsets.py [-h] {all,count,plot} ...

Tool for characterizing and plottings kmer_spectra subsets given several datasets

positional arguments:
  {all,count,plot}
    all             Quantifies kmers based on specified subsets and plots the spectra
    count           Quantifies kmers based on specified subsets
    plot            plots kmers based on provided 'count' results

options:
  -h, --help        show this help message and exit
```
